package com.soupergeng.o2o.service;

import com.soupergeng.o2o.BaseTest;
import com.soupergeng.o2o.entity.Area;
import com.soupergeng.o2o.service.area.AreaService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * 测试Service层的连通性
 * Created by souper.geng on 2018/11/7.
 */
public class AreaServiceTest extends BaseTest {
    @Autowired
    private AreaService areaService;

    @Test
    public void testQueryList(){
        List<Area> areaList = areaService.getAreaList();
        assertEquals("西苑",areaList.get(0).getAreaName());
    }
}
