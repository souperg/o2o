package com.soupergeng.o2o.service;

import com.soupergeng.o2o.BaseTest;
import com.soupergeng.o2o.dto.ShopExecution;
import com.soupergeng.o2o.entity.Area;
import com.soupergeng.o2o.entity.PersonInfo;
import com.soupergeng.o2o.entity.Shop;
import com.soupergeng.o2o.entity.ShopCategory;
import com.soupergeng.o2o.enums.ShopStateEnum;
import com.soupergeng.o2o.service.shop.ShopService;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * 测试Service层的连通性
 * Created by souper.geng on 2018/11/7.
 */
public class ShopServiceTest extends BaseTest {
    @Autowired
    private ShopService shopService;

    @Test
    @Ignore
    public void test() throws Exception {
        Shop shop = new Shop();
        PersonInfo personInfo = new PersonInfo();
        Area area = new Area();
        ShopCategory shopCategory = new ShopCategory();
        personInfo.setUserId(1L);
        area.setAreaId(2);
        shopCategory.setShopCategoryId(1L);
        shop.setPersonInfo(personInfo);
        shop.setArea(area);
        shop.setShopCategory(shopCategory);
        shop.setAdvice("审核中");
        shop.setCreateTime(new Date());
        shop.setPhone("17600220510");
        shop.setPriority(1);
        shop.setShopAddr("test");
        shop.setShopDesc("test");
        shop.setShopImg("test");
        shop.setShopName("test2的店铺");
        shop.setEnableStatus(ShopStateEnum.CHECK.getState());
        String imgpath = "D:/app/image/sky.jpg";
        ShopExecution execution = shopService.addShop(shop, fileToMutiFile(imgpath));
        assertEquals(ShopStateEnum.CHECK.getState(),execution.getState());
    }

    /**
     * 路径转换成CommonsMultipartFile
     * @param imgpath
     * @return
     */
    private MultipartFile fileToMutiFile(String imgpath) throws Exception {
        File file  = new File(imgpath);
        Path path = Paths.get(imgpath);
        String name = "sky.jpg";
        String originalFileName = "sky.jpg";
        String contentType = "text/plain";
        byte[] content = Files.readAllBytes(path);
        MultipartFile multipartFile = new MockMultipartFile(name,originalFileName,contentType,content);
        return multipartFile;
    }
}
