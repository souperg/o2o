package com.soupergeng.o2o.service;

import com.soupergeng.o2o.BaseTest;
import com.soupergeng.o2o.entity.Product;
import com.soupergeng.o2o.entity.ProductCategory;
import com.soupergeng.o2o.entity.Shop;
import com.soupergeng.o2o.enums.ProductStateEnum;
import com.soupergeng.o2o.exception.ProductExecution;
import com.soupergeng.o2o.service.product.ProductService;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author souper.geng
 * @date 2019/3/14
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductServiceTest extends BaseTest {

    @Autowired
    private ProductService productService;

    @Test
    public void testAddProduct() throws Exception{
        Product product = new Product();
        Shop shop = new Shop();
        shop.setShopId(8L);
        ProductCategory productCategory = new ProductCategory();
        productCategory.setProductCategoryId(5L);
        product.setShop(shop);
        product.setProductCategory(productCategory);
        product.setProductName("测试商品");
        product.setProductDesc("测试商品111z");
        product.setPriority(20);
        product.setCreateTime(new Date());
        product.setEnableStatus(ProductStateEnum.SUCCESS.getState());
        //创建文件
//        String imgpath = "D:/app/image/sky.jpg"; //windows
        String imgpath = "/Users/souper.geng/Pictures/image-1.jpg"; //mac
        MultipartFile thumbnail = fileToMutiFile(imgpath);
        MultipartFile file1 = fileToMutiFile(imgpath);
        String imgpath2 = "/Users/souper.geng/Pictures/caffee.jpg";
        MultipartFile file2 = fileToMutiFile(imgpath2);

        List<MultipartFile> multipartFileList = new ArrayList<>();

        multipartFileList.add(file1);
        multipartFileList.add(file2);
        ProductExecution productExecution = productService.addProduct(product,thumbnail,multipartFileList);

        assertEquals(productExecution.getState(),ProductStateEnum.SUCCESS.getState());
    }

    @Test
    public void testBModifiedProduct() throws Exception{
        Product product = new Product();
        Shop shop = new Shop();
        shop.setShopId(8L);
        ProductCategory productCategory = new ProductCategory();
        productCategory.setProductCategoryId(5L);
        product.setProductId(2l);
        product.setShop(shop);
        product.setProductCategory(productCategory);
        product.setProductName("测试商品");
        product.setProductDesc("测试商品111z");
        product.setPriority(20);
        product.setCreateTime(new Date());
        product.setEnableStatus(ProductStateEnum.SUCCESS.getState());
        //创建文件
//        String imgpath = "D:/app/image/sky.jpg"; //windows
        String imgpath = "/Users/souper.geng/Pictures/image-1.jpg"; //mac
        MultipartFile thumbnail = fileToMutiFile(imgpath);
        MultipartFile file1 = fileToMutiFile(imgpath);
        String imgpath2 = "/Users/souper.geng/Pictures/caffee.jpg";
        MultipartFile file2 = fileToMutiFile(imgpath2);

        List<MultipartFile> multipartFileList = new ArrayList<>();

        multipartFileList.add(file1);
        multipartFileList.add(file2);
        ProductExecution productExecution = productService.modifyProduct(product,thumbnail,multipartFileList);

        assertEquals(productExecution.getState(),ProductStateEnum.SUCCESS.getState());
    }

    /**
     * 路径转换成CommonsMultipartFile
     * @param imgpath
     * @return
     */
    private MultipartFile fileToMutiFile(String imgpath) throws Exception {
        File file  = new File(imgpath);
        Path path = Paths.get(imgpath);
        String name = "sky.jpg";
        String originalFileName = "sky.jpg";
        String contentType = "text/plain";
        byte[] content = Files.readAllBytes(path);
        MultipartFile multipartFile = new MockMultipartFile(name,originalFileName,contentType,content);
        return multipartFile;
    }
}
