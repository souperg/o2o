package com.soupergeng.o2o.selftest;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

/**
 * Created by SouperGkz on 2019/1/9.
 */
public class ReflectTest {
    public static void main(String[] args) throws Exception {
        Constructor constructor = ReflectA.class.getConstructor();
        Object object = constructor.newInstance();
        Method[] methods = ReflectA.class.getDeclaredMethods();
        for (Method m :
                methods) {
            if ("say".equals(m.getName())) {
                m.invoke(object);
            }
        }
    }
}
class ReflectA{
    private int number = 1;
    public static int count = 2;

    public ReflectA() {
        System.out.println("无参构造函数");
    }

    public ReflectA(int number) {
        this.number = number;
        System.out.println("带参构造函数"+number);
    }

    public void say(){
        System.out.println("say 方法");
    }
}
