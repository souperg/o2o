package com.soupergeng.o2o.selftest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SouperGkz on 2019/1/15.
 */
public class ListTest {
    public static void main(String[] args) {
        List<Object> objectList = new ArrayList<>();
        List<A> aList = new ArrayList<>();
        List<B> bList = new ArrayList<>();
        objectList.add(0,bList);
        System.out.println(objectList.size());
    }

}
class A {
    long id;

    public A() {
    }
}

class B {
    long id;

    public B() {
    }
}
