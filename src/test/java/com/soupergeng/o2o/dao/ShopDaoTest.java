package com.soupergeng.o2o.dao;

import com.soupergeng.o2o.BaseTest;
import com.soupergeng.o2o.dao.shop.ShopDao;
import com.soupergeng.o2o.entity.Area;
import com.soupergeng.o2o.entity.PersonInfo;
import com.soupergeng.o2o.entity.Shop;
import com.soupergeng.o2o.entity.ShopCategory;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * ShopDao测试类
 * Created by souper.geng on 2018/11/12.
 */
public class ShopDaoTest extends BaseTest {

    @Autowired
    private ShopDao shopDao;

    @Test
    public void testInsertShop() {
        Shop shop = new Shop();
        PersonInfo personInfo = new PersonInfo();
        Area area = new Area();
        ShopCategory shopCategory = new ShopCategory();
        personInfo.setUserId(1L);
        area.setAreaId(2);
        shopCategory.setShopCategoryId(1L);
        shop.setPersonInfo(personInfo);
        shop.setArea(area);
        shop.setShopCategory(shopCategory);
        shop.setAdvice("审核中");
        shop.setCreateTime(new Date());
        shop.setPhone("17600220510");
        shop.setPriority(1);
        shop.setShopAddr("test");
        shop.setShopDesc("test");
        shop.setShopImg("test");
        shop.setShopName("test的店铺4");
        shop.setEnableStatus(1);

        int result = shopDao.insertShop(shop);

        assertEquals(1,result);

    }


    @Ignore
    @Test
    public void testUpdateShop() {
        Shop shop = new Shop();
        shop.setShopId(1L);
        shop.setShopAddr("北京");
        shop.setShopDesc("好吃");
        shop.setLastEditTime(new Date());
        int result = shopDao.updateShop(shop);
        assertEquals(1,result);

    }

    @Test
    public void listByShopId() {
        Shop shop = shopDao.queryByShopId(8L);
        System.out.println(shop.getArea().getAreaId());
        System.out.println(shop.getArea().getAreaName());

    }

    @Test
    public void queryShopList() {

        Shop shop = new Shop();
        PersonInfo personInfo = new PersonInfo();
        personInfo.setUserId(1L);
        shop.setPersonInfo(personInfo);
        List<Shop> list = shopDao.queryShopList(shop,0,5);
        int size = shopDao.queryShopCount(shop);
        System.out.println(list.size()+"   "+size);

    }
}
