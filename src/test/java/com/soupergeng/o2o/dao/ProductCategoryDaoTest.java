package com.soupergeng.o2o.dao;

import com.soupergeng.o2o.BaseTest;
import com.soupergeng.o2o.dao.product.ProductCategoryDao;
import com.soupergeng.o2o.entity.ProductCategory;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

/**
 * AreaDao测试类
 * Created by souper.geng on 2018/11/7.
 */
public class ProductCategoryDaoTest extends BaseTest {

    @Autowired
    private ProductCategoryDao productCategoryDao;

    @Test
    @Ignore
    public void testQuery(){
        Long shopId = 8L;
        List<ProductCategory> list = productCategoryDao.queryProductCategory(shopId);
        assertEquals(4,list.size());
    }

    @Test
    public void batchInsert(){
        ProductCategory p1 = new ProductCategory();
        ProductCategory p2 = new ProductCategory();
        p1.setProductCategoryName("p1");
        p2.setProductCategoryName("p2");
        p1.setPriority(1);
        p2.setPriority(2);
        p1.setCreateTime(new Date());
        p2.setCreateTime(new Date());
        p1.setShopId(8L);
        p2.setShopId(8L);
        List<ProductCategory> list = new ArrayList<>();
        list.add(p1);
        list.add(p2);

        int result = productCategoryDao.bacthInsertProductCategory(list);

        assertEquals(2,result);

    }
}
