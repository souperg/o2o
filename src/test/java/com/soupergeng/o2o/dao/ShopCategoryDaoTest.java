package com.soupergeng.o2o.dao;

import com.soupergeng.o2o.BaseTest;
import com.soupergeng.o2o.dao.shop.ShopCategoryDao;
import com.soupergeng.o2o.entity.ShopCategory;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * AreaDao测试类
 * Created by souper.geng on 2018/11/7.
 */
public class ShopCategoryDaoTest extends BaseTest {

    @Autowired
    private ShopCategoryDao shopCategoryDao;

    @Test
    public void testQuery(){
        ShopCategory shopCategory = new ShopCategory();
        ShopCategory parentShopCategory = new ShopCategory();
        parentShopCategory.setShopCategoryId(1L);
        shopCategory.setParent(parentShopCategory);
        List<ShopCategory> areaList = shopCategoryDao.queryShopCategory(shopCategory);
        assertEquals(1,areaList.size());
    }
}
