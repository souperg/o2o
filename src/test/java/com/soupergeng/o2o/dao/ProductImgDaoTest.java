package com.soupergeng.o2o.dao;

import com.soupergeng.o2o.BaseTest;
import com.soupergeng.o2o.dao.product.ProductImgDao;
import com.soupergeng.o2o.entity.ProductImg;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

/**
 * @author souper.geng
 * @date 2019/3/13
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductImgDaoTest extends BaseTest {
    @Autowired
    private ProductImgDao productImgDao;
    @Test
    public void testABatchInsertProducImg() throws Exception{
        //productId 为1的添加两张图片商品详情
        List<ProductImg> list = new ArrayList<>();
        ProductImg productImg = new ProductImg();
        productImg.setImgAddr("图片1");
        productImg.setImgDesc("图片1");
        productImg.setPriority(1);
        productImg.setCreateTime(new Date());
        productImg.setProductId(2L);
        list.add(productImg);
        productImg.setImgAddr("图片2");
        productImg.setImgDesc("图片2");
        productImg.setPriority(2);
        productImg.setCreateTime(new Date());
        productImg.setProductId(2L);
        list.add(productImg);
        int result = productImgDao.batchInsertProductImg(list);

        assertEquals(2,result);
    }
    @Test
    public void testBQueryProductImgList(){
        long productId = 6l;
        List<ProductImg> productImgList= productImgDao.queryProductImgList(productId);
        assertEquals(productImgList.size(),2);
    }

    @Test
    public void testCDeleteProductImgByProductId() throws Exception{
        long productId = 2l;
        int result = productImgDao.deleteProductImgById(productId);
        assertEquals(result,2);
    }

}
