package com.soupergeng.o2o.dao;

import com.soupergeng.o2o.BaseTest;
import com.soupergeng.o2o.dao.area.AreaDao;
import com.soupergeng.o2o.entity.Area;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * AreaDao测试类
 * Created by souper.geng on 2018/11/7.
 */
public class AreaDaoTest extends BaseTest {

    @Autowired
    private AreaDao areaDao;

    @Test
    public void testQueryArea(){
        List<Area> areaList = areaDao.queryArea();
        assertEquals(2,areaList.size());
    }
}
