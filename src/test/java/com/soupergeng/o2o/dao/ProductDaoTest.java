package com.soupergeng.o2o.dao;

import com.soupergeng.o2o.BaseTest;
import com.soupergeng.o2o.dao.product.ProductDao;
import com.soupergeng.o2o.dao.product.ProductImgDao;
import com.soupergeng.o2o.entity.Product;
import com.soupergeng.o2o.entity.ProductCategory;
import com.soupergeng.o2o.entity.Shop;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author souper.geng
 * @date 2019/3/13
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductDaoTest extends BaseTest {
    @Autowired
    private ProductDao productDao;

    @Autowired
    private ProductImgDao productImgDao;

    @Test
    public void testAInsertProduct() throws Exception {
        Product product = new Product();
        product.setCreateTime(new Date());
        product.setEnableStatus(1);
        product.setPriority(1);
        product.setProductName("奶茶");
        product.setNormalPrice(5.0);
        product.setPromotionPrice(6.0);
        product.setImgAddr("图片");
        Shop shop = new Shop();
        shop.setShopId(8L);
        product.setShop(shop);
        product.setProductDesc("奶茶妹妹的小茶店");
        int result = productDao.insertProduct(product);
        assertEquals(1, result);
    }

    @Test
    public void testBQueryProductList() throws Exception {
        Product p = new Product();
        //分页查询,逾期返回三条结果
        List<Product> list = productDao.queryProductList(p,0,2);
        assertEquals(list.size(),2);
        //查询名称为测试的商品总数
        int count = productDao.queryProductCount(p);
        assertEquals(3,count);
        //使用名称模糊查询
        p.setProductName("测试");
        list = productDao.queryProductList(p,0,2);
        assertEquals(list.size(),2);
        count = productDao.queryProductCount(p);
        assertEquals(2,count);
    }


    @Test
    public void testCQueryProductByPriductId() throws Exception {

        //查询productId = 2 的商品详情
        Product p = productDao.queryProductById(2l);
        assertEquals(2, p.getProductImgList().size());

    }

    @Test
    public void testDUpdateProduct() throws Exception {
        Product p = new Product();
        ProductCategory pc = new ProductCategory();
        Shop shop = new Shop();
        shop.setShopId(8L);
        pc.setProductCategoryId(5l);
        p.setProductId(6l);
        p.setShop(shop);
        p.setProductCategory(pc);
        p.setProductName("一个一个的商品");

        int result = productDao.updateProduct(p);
        assertEquals(result, 1);
    }
}
