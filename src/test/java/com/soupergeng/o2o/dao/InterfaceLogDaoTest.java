package com.soupergeng.o2o.dao;

import com.soupergeng.o2o.BaseTest;
import com.soupergeng.o2o.dao.shop.ShopDao;
import com.soupergeng.o2o.entity.*;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * ShopDao测试类
 * Created by souper.geng on 2018/11/12.
 */
public class InterfaceLogDaoTest extends BaseTest {

    @Autowired
    private InterfaceLogDao interfaceLogDao;

    @Test
    public void testInsert() {

        InterfaceLog log = new InterfaceLog();
        log.setInterfaceName("/shopadmin/shopoperation");
        log.setUsedTime(1000L);
        log.setCreateTime(new Date());

        int result = interfaceLogDao.insertLog(log);

        assertEquals(1,result);

    }


}
