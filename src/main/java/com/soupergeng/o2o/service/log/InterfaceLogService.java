package com.soupergeng.o2o.service.log;

import com.soupergeng.o2o.entity.InterfaceLog;

/**
 *
 * Created by souper.geng on 2018/11/23.
 */
public interface InterfaceLogService {
    int insertLog(InterfaceLog interfaceLog);
}
