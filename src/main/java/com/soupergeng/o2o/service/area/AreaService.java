package com.soupergeng.o2o.service.area;

import com.soupergeng.o2o.entity.Area;

import java.util.List;

/**
 *
 * Created by souper.geng on 2018/11/7.
 */
public interface AreaService {
    List<Area> getAreaList();
}
