package com.soupergeng.o2o.service.product;

import com.soupergeng.o2o.entity.Product;
import com.soupergeng.o2o.exception.ProductExecution;
import com.soupergeng.o2o.exception.ProductOperationException;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author souper.geng
 * @date 2019/3/14
 */
public interface ProductService {
    /**
     * 添加商品信息以及图片处理
     * @param product 产品
     * @param productImgList 产品详情图片
     * @param thumbnail 缩略图
     * @return
     * @throws ProductOperationException
     */
    ProductExecution addProduct(Product product,MultipartFile thumbnail, List<MultipartFile> productImgList) throws ProductOperationException;

    /**
     * 通过商品ID返回商品信息
     * @param productId
     * @return
     */
    Product getProductById(long productId);

    /**
     * 修改商品信息以及图片处理
     * @param product
     * @param thumbnail
     * @param productImgList
     * @return
     * @throws ProductOperationException
     */
    ProductExecution modifyProduct(Product product,MultipartFile thumbnail, List<MultipartFile> productImgList) throws ProductOperationException;

    /**
     * 获取商品列表
     * @param product
     * @param index
     * @param pageSize
     * @return
     */
    ProductExecution getProductList(Product product,int index,int pageSize);
}
