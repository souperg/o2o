package com.soupergeng.o2o.service.product;

import com.soupergeng.o2o.dto.ProductCategoryExecution;
import com.soupergeng.o2o.entity.ProductCategory;
import com.soupergeng.o2o.exception.ProductCategoryOperationException;

import java.util.List;

/**
 *
 * @author souper.geng
 * @date 2018/12/5
 */
public interface ProductCategoryService {
    /**
     * 根据shopId查询店铺类别
     * @param shopId
     * @return
     */
    List<ProductCategory> queryList(Long shopId);

    /**
     * 批量添加商品类别
     * @param productCategoryList
     * @return
     * @throws ProductCategoryOperationException
     */
    ProductCategoryExecution batchAddProductCategory(List<ProductCategory> productCategoryList) throws ProductCategoryOperationException;

    /**
     * 将此类别下的商品的类别id置空,再删除指定商品类别
     * @param productCategoryId
     * @param shopId
     * @return
     * @throws ProductCategoryOperationException
     */
    ProductCategoryExecution deleteProductCategory(long productCategoryId,long shopId) throws ProductCategoryOperationException;
}
