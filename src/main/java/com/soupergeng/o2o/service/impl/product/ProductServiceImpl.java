package com.soupergeng.o2o.service.impl.product;

import com.soupergeng.o2o.dao.product.ProductDao;
import com.soupergeng.o2o.dao.product.ProductImgDao;
import com.soupergeng.o2o.entity.Product;
import com.soupergeng.o2o.entity.ProductImg;
import com.soupergeng.o2o.enums.ProductStateEnum;
import com.soupergeng.o2o.exception.ProductExecution;
import com.soupergeng.o2o.exception.ProductOperationException;
import com.soupergeng.o2o.service.product.ProductService;
import com.soupergeng.o2o.util.ImageUtil;
import com.soupergeng.o2o.util.PageCalculator;
import com.soupergeng.o2o.util.PathUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author souper.geng
 * @date 2019/3/14
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductDao productDao;
    @Autowired
    private ProductImgDao productImgDao;

    /**
     * 1,处理缩略图,获取缩略图相对路径并赋值给product
     * 2,往tb_product写入商品信息,并获取productId
     * 3,结合productId批量处理商品详情图
     * 4,将商品详情图片列表批量插入tb_product_img表中
     *
     * @param product        产品
     * @param thumbnail      缩略图
     * @param productImgList 产品详情图片
     * @return ProductExecution
     * @throws ProductOperationException 异常
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ProductExecution addProduct(Product product, MultipartFile thumbnail, List<MultipartFile> productImgList) throws ProductOperationException {
        if (product != null && product.getShop().getShopId() > 0) {
            //给商品设置属性
            product.setCreateTime(new Date());
            product.setLastEditTime(new Date());
            product.setEnableStatus(1);
            product.setPriority(10);
            if (thumbnail != null) {
                addImg(product, thumbnail);
            }
            try {
                //创建商品信息
                int result = productDao.insertProduct(product);
                if (result <= 0) {
                    throw new ProductOperationException("创建商品失败");
                }
            } catch (Exception e) {
                throw new ProductOperationException("创建商品失败" + e.getMessage());
            }
            //商品图片详情不为空
            if (productImgList != null && productImgList.size() > 0) {
                addImgList(product, productImgList);
            }
            return new ProductExecution(ProductStateEnum.SUCCESS, product);
        } else {
            return new ProductExecution(ProductStateEnum.EMPTY);
        }
    }

    @Override
    public Product getProductById(long productId) {
        return productDao.queryProductById(productId);
    }

    /**
     * 1,若缩略图有值,则处理缩略图-先删除之前的缩略图,之后获取缩略图路径复制给product
     * 2,若详情图有值,同上
     * 3,将tb_product_img下的该商品原有图片删除
     * 4,更新product信息
     *
     * @param product product
     * @param thumbnail thumbnail
     * @param productImgList productImgList
     * @return ProductExecution
     * @throws ProductOperationException ProductOperationException
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ProductExecution modifyProduct(Product product, MultipartFile thumbnail, List<MultipartFile> productImgList) throws ProductOperationException {
        if (product != null && product.getShop() != null && product.getShop().getShopId() != null) {
            //设置更新时间
            product.setLastEditTime(new Date());
            //判断缩略图是否有值
            if (thumbnail != null) {
                //不为空,先删除,再添加
                Product pro = productDao.queryProductById(product.getProductId());
                if (pro.getImgAddr() != null) {
                    ImageUtil.deleteFileOrPath(pro.getImgAddr());
                }
                //添加
                addImg(product, thumbnail);
            }
            if (productImgList != null && productImgList.size() > 0) {
                //详情图片列表不为空 先删除原有
                deleteProductImgList(product.getProductId());
                //添加
                addImgList(product, productImgList);
            }
            //更新商品信息
            try {
                int result = productDao.updateProduct(product);
                if (result <= 0) {
                    throw new ProductOperationException("更新商品失败");
                } else {
                    return new ProductExecution(ProductStateEnum.SUCCESS, product);
                }
            } catch (Exception e) {
                throw new ProductOperationException("更新商品失败" + e.getMessage());
            }
        } else {
            return new ProductExecution(ProductStateEnum.EMPTY);
        }
    }

    /**
     * 获取商品列表
     * @param product
     * @param index
     * @param pageSize
     * @return
     */
    @Override
    public ProductExecution getProductList(Product product, int index, int pageSize) {
        //首先转换成数据库的行码,并调用dao层取回指定的页码的商品列表
        int rowIndex = PageCalculator.calculatorRowIndex(index,pageSize);
        List<Product> productList = productDao.queryProductList(product,rowIndex,pageSize);
        //查询商品总数
        int count = productDao.queryProductCount(product);
        if (productList!=null && productList.size()>0){
            ProductExecution productExecution = new ProductExecution();
            productExecution.setCount(count);
            productExecution.setProductList(productList);
            productExecution.setState(ProductStateEnum.SUCCESS.getState());
            return productExecution;
        } else {
            return new ProductExecution(ProductStateEnum.ERROR);
        }
    }

    /**
     * 删除该productId产品下的图片详情
     * @param productId productId
     */
    private void deleteProductImgList(Long productId) {
        //先删除图片,在更新库
        List<ProductImg> imgList = productImgDao.queryProductImgList(productId);
        for (ProductImg img :
                imgList) {
            if (img.getImgAddr() != null) {
                ImageUtil.deleteFileOrPath(img.getImgAddr());
            }
        }
        productImgDao.deleteProductImgById(productId);
    }

    /**
     * 添加图片详情
     * @param product product
     * @param productImgList productImgList
     */
    private void addImgList(Product product, List<MultipartFile> productImgList) {
        String dest = PathUtil.getShopImagePath(product.getShop().getShopId());
        //遍历图片,添加至productImg实体中
        List<ProductImg> imgList = new ArrayList<>();
        //索引代表权重值
        int index = 0;
        for (MultipartFile file : productImgList) {
            ProductImg productImg = new ProductImg();
            String imgAddress = ImageUtil.generateNormalImage(file, dest);
            productImg.setImgAddr(imgAddress);
            productImg.setProductId(product.getProductId());
            productImg.setCreateTime(new Date());
            productImg.setPriority(index++);
            imgList.add(productImg);
        }
        // 批量操作添加至数据库
        if (imgList.size() > 0) {
            try {
                int result = productImgDao.batchInsertProductImg(imgList);
                if (result <= 0) {
                    throw new ProductOperationException("批量添加商品详情图片失败");
                }
            } catch (Exception e) {
                throw new ProductOperationException("批量添加商品详情图片失败" + e.getMessage());
            }
        }
    }


    /**
     * 添加缩略图
     *
     * @param product Product
     * @param img MultipartFile
     */
    private void addImg(Product product, MultipartFile img) {
        //获取图片相对值路径
        String dest = PathUtil.getShopImagePath(product.getShop().getShopId());
        String shopImgAddr = ImageUtil.generateThumbnail(img, dest);
        product.setImgAddr(shopImgAddr);
    }
}
