package com.soupergeng.o2o.service.impl.product;

import com.soupergeng.o2o.dao.product.ProductCategoryDao;
import com.soupergeng.o2o.dto.ProductCategoryExecution;
import com.soupergeng.o2o.entity.ProductCategory;
import com.soupergeng.o2o.enums.ProductCategoryStateEnum;
import com.soupergeng.o2o.exception.ProductCategoryOperationException;
import com.soupergeng.o2o.service.product.ProductCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 *
 * Created by souper.geng on 2018/12/5.
 */
@Service
public class ProductCategoryServiceImpl implements ProductCategoryService {
    @Autowired
    ProductCategoryDao productCategoryDao;

    @Override
    public List<ProductCategory> queryList(Long shopId) {
        return productCategoryDao.queryProductCategory(shopId);
    }

    @Override
    public ProductCategoryExecution batchAddProductCategory(List<ProductCategory> productCategoryList) throws ProductCategoryOperationException {
        if (productCategoryList!=null && productCategoryList.size()>0){
            try{
                int result = productCategoryDao.bacthInsertProductCategory(productCategoryList);
                if (result<=0){
                    throw new ProductCategoryOperationException("批量添加失败");
                } else {
                    return new ProductCategoryExecution(ProductCategoryStateEnum.SUCCESS);
                }
            } catch (Exception e){
                throw new ProductCategoryOperationException("批量添加失败"+e.getMessage());
            }
        } else {
            return new ProductCategoryExecution(ProductCategoryStateEnum.EMPTY_LIST);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ProductCategoryExecution deleteProductCategory(long productCategoryId, long shopId) throws ProductCategoryOperationException {

        //TODO 将此类别下的商品的类别id置空
        try {
            int result = productCategoryDao.deleteProductCategory(productCategoryId,shopId);
            if (result<=0){
                throw new ProductCategoryOperationException("删除失败");
            } else {
                return new ProductCategoryExecution(ProductCategoryStateEnum.SUCCESS);
            }
        } catch (Exception e){
            throw new ProductCategoryOperationException("删除失败" + e.getMessage());
        }
    }
}
