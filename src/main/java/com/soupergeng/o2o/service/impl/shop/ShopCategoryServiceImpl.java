package com.soupergeng.o2o.service.impl.shop;

import com.soupergeng.o2o.dao.shop.ShopCategoryDao;
import com.soupergeng.o2o.entity.ShopCategory;
import com.soupergeng.o2o.service.shop.ShopCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 * Created by souper.geng on 2018/11/19.
 */
@Service
public class ShopCategoryServiceImpl implements ShopCategoryService {
    @Autowired
    ShopCategoryDao shopCategoryDao;
    @Override
    public List<ShopCategory> queryShopCategory(ShopCategory shopCategoryCondition) {
        return shopCategoryDao.queryShopCategory(shopCategoryCondition);
    }
}
