package com.soupergeng.o2o.service.impl.area;

import com.soupergeng.o2o.dao.area.AreaDao;
import com.soupergeng.o2o.entity.Area;
import com.soupergeng.o2o.service.area.AreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 * Created by souper.geng on 2018/11/7.
 */
@Service
public class AreaServiceImpl implements AreaService {

    @Autowired
    AreaDao areaDao;
    
    @Override
    public List<Area> getAreaList() {
        return areaDao.queryArea();
    }
}
