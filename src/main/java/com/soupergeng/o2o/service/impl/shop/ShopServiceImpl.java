package com.soupergeng.o2o.service.impl.shop;

import com.alibaba.druid.util.StringUtils;
import com.soupergeng.o2o.dao.shop.ShopDao;
import com.soupergeng.o2o.dto.ShopExecution;
import com.soupergeng.o2o.entity.Shop;
import com.soupergeng.o2o.enums.ShopStateEnum;
import com.soupergeng.o2o.exception.ShopOperationException;
import com.soupergeng.o2o.service.shop.ShopService;
import com.soupergeng.o2o.util.ImageUtil;
import com.soupergeng.o2o.util.PageCalculator;
import com.soupergeng.o2o.util.PathUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;

/**
 * Created by SouperGkz on 2018/11/13.
 */
@Service
public class ShopServiceImpl implements ShopService {
    @Autowired
    ShopDao shopDao;

    @Override
    @Transactional
    public ShopExecution addShop(Shop shop, MultipartFile shopImg) {
        if (shop == null) {
            return new ShopExecution(ShopStateEnum.NULL_SHOP);
        }

        try {
            shop.setEnableStatus(0);
            shop.setCreateTime(new Date());
            shop.setLastEditTime(new Date());
            int result = shopDao.insertShop(shop);
            if (result <= 0) {
                throw new ShopOperationException("Add Shop Error:店铺创建失败");
            }
            if (shopImg != null) {
                try {
                    //存储图片
                    addShopImg(shop, shopImg);
                } catch (Exception e) {
                    throw new ShopOperationException("Add Shop Error:" + e.getMessage());
                }
                //更新图片地址
                result = shopDao.updateShop(shop);
                if (result <= 0) {
                    throw new ShopOperationException("Add Shop Error: 图片更新失败");
                }
            }

        } catch (Exception e) {
            throw new ShopOperationException("Add Shop Error:" + e.getMessage());
        }
        return new ShopExecution(ShopStateEnum.CHECK);
    }

    @Override
    public Shop getByShopId(long shopId) {
        return shopDao.queryByShopId(shopId);
    }

    @Override
    public ShopExecution modifyShop(Shop shop, MultipartFile shopImg) {

        if (shop == null || shop.getShopId() == null) {
            return new ShopExecution(ShopStateEnum.NULL_SHOP);
        } else {
            try {
                //1.是否需要更新图片
                if (shopImg != null) {
                    Shop tempShop = shopDao.queryByShopId(shop.getShopId());
                    if (!StringUtils.isEmpty(tempShop.getShopImg())) {
                        ImageUtil.deleteFileOrPath(tempShop.getShopImg());
                    }
                    addShopImg(shop, shopImg);
                }
                //2.更新店铺消息
                shop.setLastEditTime(new Date());
                int result = shopDao.updateShop(shop);
                if (result <= 0) {
                    return new ShopExecution(ShopStateEnum.ERROR);
                } else {
                    shop = shopDao.queryByShopId(shop.getShopId());
                    return new ShopExecution(ShopStateEnum.SUCCESS, shop);
                }
            } catch (Exception e) {
                throw new ShopOperationException("modifyShop error:" + e.getMessage());
            }

        }
    }

    @Override
    public ShopExecution getShopList(Shop shopCondition, int pageIndex, int pageSize) {
        int rowIndex = PageCalculator.calculatorRowIndex(pageIndex, pageSize);
        List<Shop> list = shopDao.queryShopList(shopCondition, rowIndex, pageSize);
        int count = shopDao.queryShopCount(shopCondition);
        ShopExecution execution = new ShopExecution();
        if (list != null && list.size() != 0) {
            execution.setCount(count);
            execution.setShopList(list);
        } else {
            execution.setState(ShopStateEnum.ERROR.getState());
        }
        return execution;
    }

    private void addShopImg(Shop shop, MultipartFile shopImg) {
        //获取图片相对值路径
        String dest = PathUtil.getShopImagePath(shop.getShopId());
        String shopImgAddr = ImageUtil.generateThumbnail(shopImg, dest);
        shop.setShopImg(shopImgAddr);
    }
}
