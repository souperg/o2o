package com.soupergeng.o2o.service.impl.log;

import com.soupergeng.o2o.dao.InterfaceLogDao;
import com.soupergeng.o2o.entity.InterfaceLog;
import com.soupergeng.o2o.service.log.InterfaceLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * Created by souper.geng on 2018/11/23.
 */
@Service
public class InterfaceLogServiceImpl implements InterfaceLogService {

    @Autowired
    InterfaceLogDao interfaceLogDao;

    @Override
    public int insertLog(InterfaceLog interfaceLog) {
        return interfaceLogDao.insertLog(interfaceLog);
    }
}
