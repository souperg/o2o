package com.soupergeng.o2o.service.shop;

import com.soupergeng.o2o.dto.ShopExecution;
import com.soupergeng.o2o.entity.Shop;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * Created by SouperGkz on 2018/11/13.
 */
public interface ShopService {
    /**
     * 添加店铺信息
     * @param shop
     * @param shopImg
     * @return
     */
    ShopExecution addShop(Shop shop, MultipartFile shopImg);

    /**
     * 通过店铺id获取店铺信息
     * @param shopId
     * @return
     */
    Shop getByShopId(long shopId);

    /**
     *
     * @param shop
     * @param shopImg
     * @return
     */
    ShopExecution modifyShop(Shop shop, MultipartFile shopImg);

    /**
     * 获取商铺列表
     * @param shopCondition
     * @param pageIndex
     * @param pageSize
     * @return
     */
    ShopExecution getShopList(Shop shopCondition, int pageIndex, int pageSize);
}
