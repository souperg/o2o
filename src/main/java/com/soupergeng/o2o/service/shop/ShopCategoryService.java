package com.soupergeng.o2o.service.shop;

import com.soupergeng.o2o.entity.ShopCategory;

import java.util.List;

/**
 *
 * Created by souper.geng on 2018/11/19.
 */
public interface ShopCategoryService {
    List<ShopCategory> queryShopCategory(ShopCategory shopCategoryCondition);
}
