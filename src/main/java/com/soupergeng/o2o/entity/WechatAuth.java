package com.soupergeng.o2o.entity;

import lombok.Data;

import java.util.Date;

/**
 * 微信账号信息
 *
 * @author souper.geng
 * @date 2018/11/6
 */
@Data
public class WechatAuth {
    private Long wechatAuthId;
    private String openId;
    private Date createTime;
    private PersonInfo personInfo;
}
