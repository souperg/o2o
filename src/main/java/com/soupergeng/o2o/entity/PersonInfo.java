package com.soupergeng.o2o.entity;

import lombok.Data;

import java.util.Date;

/**
 * 个人信息
 *
 * @author souper.geng
 * @date 2018/11/6
 */
@Data
public class PersonInfo {
    private Long userId;
    private String name;
    private String profileImg;
    private String email;
    private String gender;
    private Integer enableStatus;
    private Integer userType;
    private Date createTime;
    private Date lastEditTime;
}
