package com.soupergeng.o2o.entity;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 商品
 * @author souper.geng
 * @date 2018/11/6
 */
@Data
public class Product {
    private Long productId;
    private String productName;
    private String productDesc;
    private String imgAddr;
    private Double normalPrice;
    private Double promotionPrice;

    private Integer priority;
    private Date createTime;
    private Date lastEditTime;

    /**
     * 0,xiajia
     * 1,在前端展示
     */
    private Integer enableStatus;

    private List<ProductImg> productImgList;
    private ProductCategory productCategory;
    private Shop shop;
}
