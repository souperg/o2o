package com.soupergeng.o2o.entity;

import lombok.Data;

import java.util.Date;

/**
 * 店铺
 *
 * @author souper.geng
 * @date 2018/11/6
 */
@Data
public class Shop {
    private Long shopId;
    private String shopName;
    private String shopDesc;
    private String shopAddr;
    private String phone;
    private String shopImg;
    private Integer priority;
    private Date createTime;
    private Date lastEditTime;
    private Integer enableStatus;

    private String advice;
    private Area area;
    private PersonInfo personInfo;
    private ShopCategory shopCategory;
}
