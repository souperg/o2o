package com.soupergeng.o2o.entity;

import lombok.Data;

import java.util.Date;

/**
 * 头条实体类
 * Created by souper.geng on 2018/11/6.
 */
@Data
public class HeadLine {
    private Long lineId;
    private String lineName;
    private String lineLink;
    private String lineImg;
    private Integer priority;
    private Integer enableStatus;
    private Date createTime;
    private Date lastEditTime;
}
