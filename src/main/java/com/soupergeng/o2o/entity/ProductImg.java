package com.soupergeng.o2o.entity;

import lombok.Data;

import java.util.Date;

/**
 * 商品图片详情
 *
 * @author souper.geng
 * @date 2018/11/6
 */
@Data
public class ProductImg {
    private Long productImgId;
    private String imgAddr;
    private String imgDesc;
    private Integer priority;
    private Date createTime;
    private Long productId;
}
