package com.soupergeng.o2o.entity;

import lombok.Data;

import java.util.Date;

/**
 * 接口时间
 *
 * @author souper.geng
 * @date 2018/11/23
 */
@Data
public class InterfaceLog {

    private Long logId;
    private String interfaceName;
    private Long usedTime;
    private Date createTime;
    private String note1;
    private String note2;
    private String note3;
}
