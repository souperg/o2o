package com.soupergeng.o2o.entity;

import lombok.Data;

import java.util.Date;

/**
 * 地区实体类
 * Created by souper.geng on 2018/11/6.
 */
@Data
public class Area {
    //ID
    private Integer areaId;
    //名称
    private String areaName;
    //权重
    private Integer priority;
    //创建时间
    private Date createTime;
    //更新时间
    private Date lastEditTime;
}
