package com.soupergeng.o2o.entity;

import lombok.Data;

import java.util.Date;

/**
 * 商品类别
 *
 * @author souper.geng
 * @date 2018/11/6
 */
@Data
public class ProductCategory {
    private Long productCategoryId;
    private Long shopId;
    private String productCategoryName;
    private Integer priority;
    private Date createTime;
}
