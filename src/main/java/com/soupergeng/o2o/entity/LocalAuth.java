package com.soupergeng.o2o.entity;

import lombok.Data;

import java.util.Date;

/**
 * 本地信息
 *
 * @author souper.geng
 * @date 2018/11/6
 */
@Data
public class LocalAuth {
    private Long localAuthId;
    private String username;
    private String password;
    private Date createTime;
    private Date lastEditTime;
    private PersonInfo personInfo;

}
