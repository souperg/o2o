package com.soupergeng.o2o.exception;

/**
 * @author SouperGkz
 * @date 2018/11/13
 */
public class ProductCategoryOperationException extends RuntimeException {


    private static final long serialVersionUID = -605592528724763470L;

    public ProductCategoryOperationException(String message) {
        super(message);
    }
}
