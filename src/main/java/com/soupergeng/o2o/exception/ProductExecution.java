package com.soupergeng.o2o.exception;

import com.soupergeng.o2o.entity.Product;
import com.soupergeng.o2o.enums.ProductStateEnum;

import java.util.List;

/**
 * @author souper.geng
 */
public class ProductExecution {
    /**
     * 结果状态
     */
    private int state;
    /**
     * 状态标识
     */
    private String stateInfo;
    /**
     * 商品数量
     */
    private int count;
    /**
     * 操作的商品 增删改用的
     */
    private Product product;
    /**
     * 获取的product列表 查询列表用的
     */
    private List<Product> productList;

    public ProductExecution() {
    }

    /**
     * 失败的构造器
     * @param productStateEnum
     */
    public ProductExecution(ProductStateEnum productStateEnum){
        this.state = productStateEnum.getState();
        this.stateInfo = productStateEnum.getStateInfo();
    }

    /**
     * 成功的构造器
     * @param productStateEnum
     * @param product
     */
    public ProductExecution(ProductStateEnum productStateEnum,Product product){
        this.state = productStateEnum.getState();
        this.stateInfo = productStateEnum.getStateInfo();
        this.product = product;
    }

    /**
     * 成功的构造器
     * @param productStateEnum
     * @param productList
     */
    public ProductExecution(ProductStateEnum productStateEnum,List<Product> productList){
        this.state = productStateEnum.getState();
        this.stateInfo = productStateEnum.getStateInfo();
        this.productList = productList;
    }

    public int getState() {
        return state;
    }

    public String getStateInfo() {
        return stateInfo;
    }

    public int getCount() {
        return count;
    }

    public Product getProduct() {
        return product;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setState(int state) {
        this.state = state;
    }

    public void setStateInfo(String stateInfo) {
        this.stateInfo = stateInfo;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }
}
