package com.soupergeng.o2o.exception;

/**
 *
 * Created by SouperGkz on 2018/11/13.
 */
public class ShopOperationException extends RuntimeException {
    public ShopOperationException(String message) {
        super(message);
    }
}
