package com.soupergeng.o2o.exception;

/**
 * @author souper.geng
 * @date 2019/3/14
 */
public class ProductOperationException extends RuntimeException {

    private static final long serialVersionUID = 7213953885355699118L;

    public ProductOperationException(String message) {
        super(message);
    }
}
