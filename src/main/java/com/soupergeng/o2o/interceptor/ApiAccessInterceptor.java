package com.soupergeng.o2o.interceptor;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.reflect.Method;

/**
 * 拦截Controller方法执行接口访问时长
 * 
 * @author franp
 */
public class ApiAccessInterceptor implements MethodInterceptor {


	@Override
	public Object invoke(MethodInvocation invocation) throws Throwable {
		Method method = invocation.getMethod();
		RequestMapping anotaiton_c = method.getDeclaringClass().getAnnotation(RequestMapping.class);
		String interNames_c[] = anotaiton_c.value();
		RequestMapping anotaiton_m = method.getAnnotation(RequestMapping.class);
		String interNames_m[] = anotaiton_m.value();
		
		String interName = interNames_c[0] + interNames_m[0];
		StopWatch stopWatch = new StopWatch(interName);
		try {
			stopWatch.start();
			return invocation.proceed();
		} catch (Exception ex) {
			throw ex;
		} finally {
			if (stopWatch.isRunning()) {
				stopWatch.stop();
			}
			timeLog(interName, stopWatch.getTotalTimeMillis());
		}
	}

	private void timeLog(String interName, long millTime) {

	}
}
