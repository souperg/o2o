package com.soupergeng.o2o.dto;

import com.soupergeng.o2o.entity.Shop;
import com.soupergeng.o2o.enums.ShopStateEnum;

import java.util.List;

/**
 * Created by SouperGkz on 2018/11/13.
 */
public class ShopExecution {
    //结果状态
    private int state;
    private String stateInfo;
    private int count;
    private Shop shop;
    private List<Shop> shopList;

    public ShopExecution() {
    }

    /**
     * 操作失败的构造器
     * @param shopStateEnum
     */
    public ShopExecution(ShopStateEnum shopStateEnum) {
        state = shopStateEnum.getState();
        stateInfo = shopStateEnum.getStateInfo();
    }

    /**
     * 操作成功的构造器
     * @param shopStateEnum
     * @param shop
     */
    public ShopExecution(ShopStateEnum shopStateEnum,Shop shop) {
        state = shopStateEnum.getState();
        stateInfo = shopStateEnum.getStateInfo();
        this.shop = shop;
    }

    /**
     * 操作成功的构造器
     * @param shopStateEnum
     * @param shopList
     */
    public ShopExecution(ShopStateEnum shopStateEnum,List<Shop> shopList) {
        state = shopStateEnum.getState();
        stateInfo = shopStateEnum.getStateInfo();
        this.shopList = shopList;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getStateInfo() {
        return stateInfo;
    }

    public void setStateInfo(String stateInfo) {
        this.stateInfo = stateInfo;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public List<Shop> getShopList() {
        return shopList;
    }

    public void setShopList(List<Shop> shopList) {
        this.shopList = shopList;
    }
}
