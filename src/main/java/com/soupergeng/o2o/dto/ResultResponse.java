package com.soupergeng.o2o.dto;

/**
 * 返回数据封装
 * Created by souper.geng on 2018/12/5.
 */
public class ResultResponse<T> {
    private boolean success;
    private T data;
    private String errMsg;
    private int errCode;

    public ResultResponse() {
    }

    public ResultResponse(boolean success, T data) {
        this.success = success;
        this.data = data;
    }

    public ResultResponse(boolean success, String errMsg, int errCode) {
        this.success = success;
        this.errMsg = errMsg;
        this.errCode = errCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public int getErrCode() {
        return errCode;
    }

    public void setErrCode(int errCode) {
        this.errCode = errCode;
    }
}
