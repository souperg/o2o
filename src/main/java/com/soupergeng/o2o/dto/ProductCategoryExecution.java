package com.soupergeng.o2o.dto;

import com.soupergeng.o2o.entity.ProductCategory;
import com.soupergeng.o2o.enums.ProductCategoryStateEnum;
import lombok.Data;

import java.util.List;

/**
 * 商品类别相关
 * Created by souper.geng on 2018/12/10.
 */
@Data
public class ProductCategoryExecution {
    /**
     * 结果状态
     */
    private int state;
    /**
     * 状态标识
     */
    private String stateInfo;
    /**
     * 列表
     */
    private List<ProductCategory> productCategoryList;
    public ProductCategoryExecution() {
    }

    /**
     * 操作失败时使用的构造器
     * @param stateEnum stateEnum
     */
    public ProductCategoryExecution(ProductCategoryStateEnum stateEnum) {
        this.state = stateEnum.getState();
        this.stateInfo = stateEnum.getStateInfo();
        this.stateInfo = stateEnum.getStateInfo();
    }

    /**
     * 操作成功时使用的构造器
     * @param stateEnum stateEnum
     * @param productCategoryList productCategoryList
     */
    public ProductCategoryExecution(ProductCategoryStateEnum stateEnum,List<ProductCategory> productCategoryList){
        this.state = stateEnum.getState();
        this.stateInfo = stateEnum.getStateInfo();
        this.productCategoryList = productCategoryList;
    }
}
