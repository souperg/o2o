package com.soupergeng.o2o.dao.area;

import com.soupergeng.o2o.entity.Area;

import java.util.List;

/**
 * 列出区域列表
 * Created by souper.geng on 2018/11/7.
 */
public interface AreaDao {
    /**
     * 列出区域列表
     * @return 列出区域列表
     */
    List<Area> queryArea();
}
