package com.soupergeng.o2o.dao.shop;

import com.soupergeng.o2o.entity.Shop;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 商品表
 */
public interface ShopDao {
    /**
     * 插入店铺
     * @param shop
     * @return
     */
    int insertShop(Shop shop);

    /**
     * 更新
     * @param shop
     * @return
     */
    int updateShop(Shop shop);

    /**
     * 根据shopId查询店铺
     * @param shopId
     * @return
     */
    Shop queryByShopId(Long shopId);

    /**
     * 分页查询
     * @param shopCondition
     * @param rowIndex 从第几行开始取
     * @param pageSize 返回的页数
     * @return
     */
    List<Shop> queryShopList(@Param("shopCondition") Shop shopCondition,@Param("rowIndex")int rowIndex
    ,@Param("pageSize")int pageSize);

    /**
     * 返回当前条件下的总数
     * @param shopCondition
     * @return
     */
    int queryShopCount(@Param("shopCondition") Shop shopCondition);
}