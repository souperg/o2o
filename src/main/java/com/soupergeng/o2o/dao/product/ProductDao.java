package com.soupergeng.o2o.dao.product;

import com.soupergeng.o2o.entity.Product;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author souper.geng
 * @date 2019/3/13
 */
public interface ProductDao {
    /**
     * 插入商品
     * @param product
     * @return
     */
    int insertProduct(Product product);

    /**
     * 更新商品
     * @param product
     * @return
     */
    int updateProduct(Product product);

    /**
     * 通过商品id查询商品详细信息
     * @param productId
     * @return
     */
    Product queryProductById(long productId);


    /**
     * @param productCondition Product
     * @param index int
     * @param pageSize int
     * @return List<Product>
     */
    List<Product> queryProductList(@Param("productCondition") Product productCondition,@Param("rowIndex") int index,@Param("pageSize") int pageSize);

    /**
     * 查询商品总数
     * @param productCondition Product
     * @return ints
     */
    int queryProductCount(@Param("productCondition") Product productCondition);


}
