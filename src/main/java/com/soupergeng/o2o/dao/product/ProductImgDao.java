package com.soupergeng.o2o.dao.product;

import com.soupergeng.o2o.entity.ProductImg;

import java.util.List;

/**
 * 添加商品图片接口
 * @author souper.geng
 * @date 2019/3/13
 */
public interface ProductImgDao {
    /**
     * 批量添加商品详情图片
     * @param productImgList
     * @return
     */
    int batchInsertProductImg(List<ProductImg> productImgList);

    /**
     * 删除指定商品下的所有详情图
     * @param productId
     * @return
     */
    int deleteProductImgById(long productId);

    /**
     * 返回指定productId下的所有详情图
     * @param productId
     * @return
     */
    List<ProductImg> queryProductImgList(long productId);
}
