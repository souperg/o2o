package com.soupergeng.o2o.dao.product;

import com.soupergeng.o2o.entity.ProductCategory;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 *
 *
 * @author souper.geng
 * @date 2018/12/5
 */
public interface ProductCategoryDao {
    /**
     * 根据shopId查询店铺类别列表
     * @param shopId
     * @return
     */
    List<ProductCategory> queryProductCategory(@Param(value = "shopId") Long shopId);

    /**
     * 批量添加商品类别
     * @param productCategoryList
     * @return
     */
    int bacthInsertProductCategory(List<ProductCategory> productCategoryList);

    /**
     * 删除指定商品类别
     * @param productCategoryId
     * @param shopId
     * @return
     */
    int deleteProductCategory(@Param("productCategoryId") long productCategoryId,@Param("shopId") long shopId);
}
