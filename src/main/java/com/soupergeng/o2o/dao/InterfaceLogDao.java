package com.soupergeng.o2o.dao;

import com.soupergeng.o2o.entity.InterfaceLog;

/**
 *
 * Created by souper.geng on 2018/11/23.
 */
public interface InterfaceLogDao {
    int insertLog(InterfaceLog interfaceLog);
}
