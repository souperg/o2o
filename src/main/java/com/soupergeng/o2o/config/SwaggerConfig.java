package com.soupergeng.o2o.config;

import org.springframework.context.annotation.Configuration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author souper.geng
 * @date 2019/3/8
 */

@Configuration
@EnableSwagger2
public class SwaggerConfig {
}
