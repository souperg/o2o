package com.soupergeng.o2o.controller.superadmin;

import com.soupergeng.o2o.entity.Area;
import com.soupergeng.o2o.service.area.AreaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 超级管理员的Controller
 * Created by souper.geng on 2018/11/7.
 */
@Controller
@RequestMapping("/superadmin")
public class AreaController {

    private Logger logger = LoggerFactory.getLogger(AreaController.class);

    @Autowired
    AreaService areaService;


    @RequestMapping(value = "/listarea",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> listArea(){
        long startTime = System.currentTimeMillis();
        logger.info("=====start=====");
        Map<String,Object> map = new HashMap<>();
        List<Area> list = new ArrayList<>();
        try {
            list = areaService.getAreaList();
            map.put("rows",list);
            map.put("total",list.size());

        } catch (Exception e){
            e.printStackTrace();
            map.put("success",false);
            map.put("errMsg",e.toString());
        }
        logger.error("test error");
        long endTime = System.currentTimeMillis();
        logger.debug("costTime:[{}ms]",endTime-startTime);
        logger.info("=====end=====");

        return map;

    }

}
