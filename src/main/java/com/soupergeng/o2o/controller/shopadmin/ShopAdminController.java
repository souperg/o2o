package com.soupergeng.o2o.controller.shopadmin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 商品管理Controller
 * Created by SouperGkz on 2018/11/14.
 */
@Controller
@RequestMapping(value = "/shopadmin",method = RequestMethod.GET)
public class ShopAdminController {
    @RequestMapping("/shopoperation")
    public String shopOperation(){
        String a = "12";
        Long b = Long.getLong(a);
        Long c = Long.valueOf(a);
        return "shop/shopoperation";
    }

    @RequestMapping("/shoplist")
    public String shopList(){
        return "shop/shoplist";
    }

    @RequestMapping("/shopmanagement")
    public String shopManagement(){
        return "shop/shopmanagement";
    }


}
