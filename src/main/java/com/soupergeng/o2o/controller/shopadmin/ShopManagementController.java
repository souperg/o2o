package com.soupergeng.o2o.controller.shopadmin;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.soupergeng.o2o.dto.ShopExecution;
import com.soupergeng.o2o.entity.Area;
import com.soupergeng.o2o.entity.PersonInfo;
import com.soupergeng.o2o.entity.Shop;
import com.soupergeng.o2o.entity.ShopCategory;
import com.soupergeng.o2o.enums.ShopStateEnum;
import com.soupergeng.o2o.service.area.AreaService;
import com.soupergeng.o2o.service.shop.ShopCategoryService;
import com.soupergeng.o2o.service.shop.ShopService;
import com.soupergeng.o2o.util.CodeUtil;
import com.soupergeng.o2o.util.HttpServletRequestUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 实现店铺相关
 * Created by SouperGkz on 2018/11/14.
 */
@Controller
@RequestMapping("/shopadmin")
public class ShopManagementController {

    @Autowired
    ShopService shopService;

    @Autowired
    AreaService areaService;

    @Autowired
    ShopCategoryService shopCategoryService;

    @ResponseBody
    @RequestMapping(value = "/getshopbyid", method = RequestMethod.GET)
    public Map<String, Object> getShopbyId(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        Long shopId = HttpServletRequestUtil.getLong(request, "shopId");
        if (shopId > -1) {
            try {
                Shop shop = shopService.getByShopId(shopId);
                List<Area> areaList = areaService.getAreaList();
                map.put("shop", shop);
                map.put("areaList", areaList);
                map.put("success", true);
            } catch (Exception e) {
                map.put("success", false);
                map.put("errMsg", e.getMessage());
            }

        } else {
            map.put("success", false);
            map.put("errMsg", "shopId is empty");
        }
        return map;

    }

    @RequestMapping(value = "/getshopinitinfo", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getShopInitInfo() {
        Map<String, Object> map = new HashMap<>();
        List<ShopCategory> shopCategoryList = new ArrayList<>();
        List<Area> areaList = new ArrayList<>();
        try {
            shopCategoryList = shopCategoryService.queryShopCategory(new ShopCategory());
            areaList = areaService.getAreaList();
            map.put("shopCategoryList", shopCategoryList);
            map.put("areaList", areaList);
            map.put("success", true);
        } catch (Exception e) {
            map.put("success", false);
            map.put("errMsg", e.getMessage());

        }
        return map;
    }

    @RequestMapping(value = "/registershop", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> registerShop(HttpServletRequest request) {
        //1,接收参数    转换对象
        Map<String, Object> map = new HashMap<>();
        if (!CodeUtil.checkVerifyCode(request)) {
            map.put("success", false);
            map.put("errMsg", "验证码输入错误,请重新输入");
            return map;
        }

        String shopStr = HttpServletRequestUtil.getString(request, "shopStr");
        ObjectMapper mapper = new ObjectMapper();
        Shop shop = null;
        try {
            shop = mapper.readValue(shopStr, Shop.class);
        } catch (Exception e) {
            map.put("success", false);
            map.put("errMsg", e.getMessage());
            return map;
        }


        CommonsMultipartFile shopImg = null;
        CommonsMultipartResolver resolver = new CommonsMultipartResolver(request.getSession().getServletContext());
        if (resolver.isMultipart(request)) {
            MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
            shopImg = (CommonsMultipartFile) multipartHttpServletRequest.getFile("shopImg");
        } else {
            map.put("success", false);
            map.put("errMsg", "上传图片不能为空");
            return map;
        }
        //2,注册店铺
        if (shopImg != null && shop != null) {
            PersonInfo personInfo = (PersonInfo) request.getSession().getAttribute("user");
            shop.setPersonInfo(personInfo);
            ShopExecution ex = shopService.addShop(shop, shopImg);
            if (ex.getState() == ShopStateEnum.CHECK.getState()) {
                map.put("success", true);
                //该用户下可操作的店铺列表
                List<Shop> shopList = (List<Shop>) request.getSession().getAttribute("shopList");
                if (shopList == null || shopList.size() == 0) {
                    shopList = new ArrayList<>();
                }
                shopList.add(ex.getShop());
                request.getSession().setAttribute("shopList", shopList);
            } else {
                map.put("success", false);
                map.put("errMsg", ex.getStateInfo());
            }
            return map;
        } else {
            map.put("success", false);
            map.put("errMsg", "店铺信息不能为空");
            return map;
        }
    }

    @ResponseBody
    @RequestMapping(value = "/updateshop", method = RequestMethod.POST)
    public Map<String, Object> updateShop(HttpServletRequest request) {
        //1,接收参数    转换对象
        Map<String, Object> map = new HashMap<>();
        if (!CodeUtil.checkVerifyCode(request)) {
            map.put("success", false);
            map.put("errMsg", "验证码输入错误,请重新输入");
            return map;
        }

        String shopStr = HttpServletRequestUtil.getString(request, "shopStr");
        ObjectMapper mapper = new ObjectMapper();
        Shop shop = null;
        try {
            shop = mapper.readValue(shopStr, Shop.class);
        } catch (Exception e) {
            map.put("success", false);
            map.put("errMsg", e.getMessage());
            return map;
        }


        CommonsMultipartFile shopImg = null;
        CommonsMultipartResolver resolver = new CommonsMultipartResolver(request.getSession().getServletContext());
        if (resolver.isMultipart(request)) {
            MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
            shopImg = (CommonsMultipartFile) multipartHttpServletRequest.getFile("shopImg");
        }
        //2,修改店铺信息
        if (shop != null && shop.getShopId() != null) {
            ShopExecution ex = shopService.modifyShop(shop, shopImg);
            if (ex.getState() == ShopStateEnum.SUCCESS.getState()) {
                map.put("success", true);
            } else {
                map.put("success", false);
                map.put("errMsg", ex.getStateInfo());
            }
            return map;
        } else {
            map.put("success", false);
            map.put("errMsg", "店铺ID不能为空");
            return map;
        }
    }

    /**
     * 根据用户信息返回用户创建的商铺信息
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getshoplist", method = RequestMethod.GET)
    public Map<String, Object> getShopList(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        PersonInfo personInfo = new PersonInfo();
        personInfo.setUserId(1L);
        personInfo.setName("郝蝶");
        request.getSession().setAttribute("user", personInfo);
        personInfo = (PersonInfo) request.getSession().getAttribute("user");
        try {
            Shop shop = new Shop();
            shop.setPersonInfo(personInfo);
            ShopExecution se = shopService.getShopList(shop, 0, 100);
            map.put("success", true);
            map.put("user", personInfo);
            map.put("shopList", se.getShopList());
        } catch (Exception e) {
            map.put("success", false);
            map.put("errMsg", e.getMessage());
        }
        return map;

    }

    /**
     * session操作
     *
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getshopmanagermentinfo", method = RequestMethod.GET)
    public Map<String, Object> getShopManagermentInfo(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        Long id = HttpServletRequestUtil.getLong(request, "shopId");
        if (id <= 0) {
            Object currentShop = request.getSession().getAttribute("currentShop");
            if (null == currentShop) {
                map.put("redirect", true);
                map.put("url", "/o2o/shop/shoplist");
            } else {
                Shop shop = (Shop) currentShop;
                map.put("redirect", false);
                map.put("shopId", shop.getShopId());
            }
        } else {
            Shop currentShop = new Shop();
            currentShop.setShopId(id);
            request.getSession().setAttribute("currentShop", currentShop);
            map.put("redirect", false);
        }
        return map;

    }
}
