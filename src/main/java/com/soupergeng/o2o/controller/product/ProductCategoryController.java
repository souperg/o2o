package com.soupergeng.o2o.controller.product;

import com.soupergeng.o2o.dto.ProductCategoryExecution;
import com.soupergeng.o2o.dto.ResultResponse;
import com.soupergeng.o2o.entity.ProductCategory;
import com.soupergeng.o2o.entity.Shop;
import com.soupergeng.o2o.enums.ProductCategoryStateEnum;
import com.soupergeng.o2o.service.product.ProductCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author souper.geng
 * @date 2018/12/5
 */
@Controller
@RequestMapping("/productcategory")
public class ProductCategoryController {
    @Autowired
    ProductCategoryService productCategoryService;

    @ResponseBody
    @RequestMapping(value = "/getproductcategorylist", method = RequestMethod.GET)
    public ResultResponse<List<ProductCategory>> getProductCategory(HttpServletRequest request) {
        ResultResponse<List<ProductCategory>> map = new ResultResponse<List<ProductCategory>>();
        // Todo 待删除
        Shop shop = new Shop();
        shop.setShopId(8L);
        request.getSession().setAttribute("currentShop",shop);

        Shop currentShop = (Shop) request.getSession().getAttribute("currentShop");
        Long shopId = currentShop.getShopId();
        if (currentShop!=null && shopId>0){
            List<ProductCategory> list = productCategoryService.queryList(shopId);
            map.setSuccess(true);
            map.setData(list);
        } else {
            map.setSuccess(false);
            map.setErrCode(ProductCategoryStateEnum.ERROR.getState());
            map.setErrMsg(ProductCategoryStateEnum.ERROR.getStateInfo());
        }

        return map;
    }

    @RequestMapping(value = "/shopcategory")
    public String shopCategory(HttpServletRequest request) {
        return "shop/productcategory";
    }

    /**
     * 批量添加商品类别接口
     * @return
     */
    @RequestMapping(value = "/addproductcategorys",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> addProductCategorys(@RequestBody List<ProductCategory> productCategoryList,HttpServletRequest request){
        Map<String,Object> modelMap = new HashMap<>();
        Shop shop = (Shop) request.getSession().getAttribute("currentShop");
        for (ProductCategory pc :
                productCategoryList) {
            pc.setShopId(shop.getShopId());
        }
        if (productCategoryList != null && productCategoryList.size()>0){
            try{
                ProductCategoryExecution pe = productCategoryService.batchAddProductCategory(productCategoryList);
                if (pe.getState() == ProductCategoryStateEnum.SUCCESS.getState()){
                    modelMap.put("success",true);
                } else {
                    modelMap.put("success",false);
                    modelMap.put("errMsg",pe.getStateInfo());
                }
            } catch (Exception e){
                modelMap.put("success",false);
                modelMap.put("errMsg",e.getMessage());
            }
        } else {
            modelMap.put("success",false);
            modelMap.put("errMsg","请输入至少一个商铺类别");
        }
        return modelMap;
    }

    /**
     * 批量添加商品类别接口
     * @return
     */
    @RequestMapping(value = "/removeproductcategory",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> removeProductCategory(Long productCategoryId,HttpServletRequest request){
        Map<String,Object> modelMap = new HashMap<>();
        Shop shop = (Shop) request.getSession().getAttribute("currentShop");

        if (productCategoryId != null && productCategoryId>0){
            try{
                ProductCategoryExecution pe = productCategoryService.deleteProductCategory(productCategoryId,shop.getShopId());
                if (pe.getState() == ProductCategoryStateEnum.SUCCESS.getState()){
                    modelMap.put("success",true);
                } else {
                    modelMap.put("success",false);
                    modelMap.put("errMsg",pe.getStateInfo());
                }
            } catch (Exception e){
                modelMap.put("success",false);
                modelMap.put("errMsg",e.getMessage());
            }
        } else {
            modelMap.put("success",false);
            modelMap.put("errMsg","请输入至少一个商铺类别");
        }
        return modelMap;
    }
}
