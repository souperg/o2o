package com.soupergeng.o2o.controller.product;

import com.alibaba.druid.util.StringUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.soupergeng.o2o.entity.Product;
import com.soupergeng.o2o.entity.ProductCategory;
import com.soupergeng.o2o.entity.Shop;
import com.soupergeng.o2o.enums.ProductStateEnum;
import com.soupergeng.o2o.exception.ProductExecution;
import com.soupergeng.o2o.service.product.ProductCategoryService;
import com.soupergeng.o2o.service.product.ProductService;
import com.soupergeng.o2o.util.CodeUtil;
import com.soupergeng.o2o.util.HttpServletRequestUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author souper.geng
 * @date 2019/3/13
 */
@Controller
@RequestMapping("/product")
public class ProductController {
    @Autowired
    private ProductService productService;
    @Autowired
    private ProductCategoryService productCategoryService;

    /**
     * 定义上传图片最大值
     */
    public static final int IMAGEMAXCOUNT = 6;

    /**
     * 商品添加或编辑页面
     *
     * @return
     */
    @RequestMapping("/productmanage")
    public String productMangement() {
        return "shop/productoperation";
    }

    /**
     * 商品列表
     *
     * @return
     */
    @RequestMapping("/productlist")
    public String productList() {
        return "shop/productlist";
    }

    /**
     * 商品添加逻辑
     *
     * @param request
     * @return
     */
    @PostMapping("/addproduct")
    @ResponseBody
    public Map<String, Object> addProducts(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        //校验码校验
        if (!CodeUtil.checkVerifyCode(request)) {
            map.put("success", false);
            map.put("errMsg", "校验码不正确");
            return map;
        }
        // 变量初始化
        ObjectMapper objectMapper = new ObjectMapper();
        Product product = null;
        String productStr = HttpServletRequestUtil.getString(request, "productStr");
        MultipartHttpServletRequest multipartHttpServletRequest = null;
        CommonsMultipartFile thumbnail = null;
        List<MultipartFile> multipartFileList = new ArrayList<>();
        CommonsMultipartResolver resolver = new CommonsMultipartResolver(request.getSession().getServletContext());
        try {
            //请求中存在文件流
            if (resolver.isMultipart(request)) {

                multipartHttpServletRequest = (MultipartHttpServletRequest) request;
                //取出缩略图
                thumbnail = (CommonsMultipartFile) multipartHttpServletRequest.getFile("thumbnail");
                //取出详情图
                for (int i = 0; i < IMAGEMAXCOUNT; i++) {
                    CommonsMultipartFile file = (CommonsMultipartFile) multipartHttpServletRequest.getFile("productImg" + i);
                    if (file != null) {
                        multipartFileList.add(file);
                    } else {
                        break;
                    }
                }
            } else {
                map.put("success", false);
                map.put("errMsg", "上传图片不能为空");
                return map;
            }
        } catch (Exception e) {
            map.put("success", false);
            map.put("errMsg", e.getMessage());
            return map;
        }
        //尝试获取前端传过来的表单String流并将其转换成Productshitil
        try {
            product = objectMapper.readValue(productStr, Product.class);
        } catch (Exception e) {
            map.put("success", false);
            map.put("errMsg", e.getMessage());
            return map;
        }
        //执行添加操作
        if (product != null && thumbnail != null && multipartFileList.size() > 0) {
            try {
                //从Session中获取当前店铺的shopId
                Shop currentShop = (Shop) request.getSession().getAttribute("currentShop");
                product.setShop(currentShop);
                //执行数据库添加操作
                ProductExecution pe = productService.addProduct(product, thumbnail, multipartFileList);
                if (pe.getState() == ProductStateEnum.SUCCESS.getState()) {
                    map.put("success", true);
                } else {
                    map.put("success", false);
                    map.put("errMsg", pe.getStateInfo());
                }
            } catch (Exception e) {
                map.put("success", false);
                map.put("errMsg", e.getMessage());
                return map;
            }

        }
        return map;

    }

    /**
     * 根据productID获取产品信息
     *
     * @param productId Long
     * @return Map<String,Object>
     */
    @ResponseBody
    @GetMapping("/getproductbyid")
    public Map<String, Object> getProductById(@RequestParam Long productId) {
        Map<String, Object> response = new HashMap<>();
        if (productId > -1) {
            //获取商品
            Product product = productService.getProductById(productId);
            if (product != null && product.getShop() != null && product.getShop().getShopId() != null) {
                //获取该店铺下的商品类别列表
                List<ProductCategory> productCategoryList = productCategoryService.queryList(product.getShop().getShopId());
                response.put("product", product);
                response.put("productCategoryList", productCategoryList);
                response.put("success", true);
            } else {
                response.put("success", false);
                response.put("errMsg", "没有该商品");
            }

        } else {
            response.put("success", false);
            response.put("errMsg", "empty productId");
        }
        return response;
    }

    /**
     * 商品修改接口
     *
     * @param request
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping("/modifyproduct")
    public Map<String, Object> modifyProduct(HttpServletRequest request) throws Exception {
        Map<String, Object> response = new HashMap<>();
        //是商品编辑的时候调用还是上下架操作的时候调用
        //若为前者则进行选择验证码判断,后者则跳过验证码判断
        boolean statusChange = HttpServletRequestUtil.getBoolean(request, "statusChange");
        //验证码判断
        if (!statusChange && !CodeUtil.checkVerifyCode(request)) {
            response.put("success", false);
            response.put("errMsg", "输入了错误的验证码");
            return response;
        }
        //接收前端参数的变量初始化,包括商品,缩略图,详情图列表实体类
        ObjectMapper mapper = new ObjectMapper();
        Product product = null;
        MultipartFile thumbnail = null;
        List<MultipartFile> multipartFiles = new ArrayList<>();
        CommonsMultipartResolver resolver = new CommonsMultipartResolver(request.getSession().getServletContext());
        try {
            //请求中存在文件流
            if (resolver.isMultipart(request)) {

                MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
                //取出缩略图
                thumbnail = multipartHttpServletRequest.getFile("thumbnail");
                //取出详情图
                for (int i = 0; i < IMAGEMAXCOUNT; i++) {
                    CommonsMultipartFile file = (CommonsMultipartFile) multipartHttpServletRequest.getFile("productImg" + i);
                    if (file != null) {
                        multipartFiles.add(file);
                    } else {
                        break;
                    }
                }
            }
        } catch (Exception e) {
            response.put("success", false);
            response.put("errMsg", e.getMessage());
            return response;
        }
        //转换实体类
        String productStr = HttpServletRequestUtil.getString(request, "productStr");
        product = mapper.readValue(productStr, Product.class);
        //执行更新操作
        if (product != null) {
            try {
                //从Session中获取当前店铺的shopId
                Shop currentShop = (Shop) request.getSession().getAttribute("currentShop");
                product.setShop(currentShop);
                //执行数据库添加操作
                ProductExecution pe = productService.modifyProduct(product, thumbnail, multipartFiles);
                if (pe.getState() == ProductStateEnum.SUCCESS.getState()) {
                    response.put("success", true);
                } else {
                    response.put("success", false);
                    response.put("errMsg", pe.getStateInfo());
                }
            } catch (Exception e) {
                response.put("success", false);
                response.put("errMsg", e.getMessage());
                return response;
            }

        }
        return response;
    }

    /**
     * @param request
     * @return
     */
    @GetMapping("/getproductlistbyshop")
    @ResponseBody
    private Map<String, Object> getProductListByShop(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        //获取前台传过来的页码
        int pageIndex = HttpServletRequestUtil.getInt(request, "pageIndex");
        //获取前台传过来每页要展示的个数
        int pageSize = HttpServletRequestUtil.getInt(request, "pageSize");
        //获取当前Shop信息
        Shop currentShop = (Shop) request.getSession().getAttribute("currentShop");
        //逻辑判断
        if (pageIndex >= 0 && pageSize >= 0 && currentShop != null && currentShop.getShopId() != null) {
            //获取传入的需要检索的条件,包括是否需要从某个商品类别以及模糊查询商品名去筛选某个店铺下的商品列表
            // 筛选条件可怕排列组合
            long productCategoryId = HttpServletRequestUtil.getLong(request, "productCategoryId");
            String productName = HttpServletRequestUtil.getString(request, "productName");
            Product product = compactProductCondition(currentShop.getShopId(), productCategoryId, productName);
            ProductExecution pe = productService.getProductList(product, pageIndex, pageSize);
            map.put("productList", pe.getProductList());
            map.put("count", pe.getCount());
            map.put("success", true);
        } else {
            map.put("success", false);
            map.put("errMsg", "非法数值");
        }
        return map;
    }

    private Product compactProductCondition(Long shopId, Long productCategoryId, String productName) {
        Product product = new Product();
        Shop shop = new Shop();
        shop.setShopId(shopId);
        product.setShop(shop);
        if (!StringUtils.isEmpty(productName)) {
            product.setProductName(productName);
        }
        if (productCategoryId != -1L) {
            ProductCategory pc = new ProductCategory();
            pc.setProductCategoryId(productCategoryId);
            product.setProductCategory(pc);
        }
        return product;
    }
}
