package com.soupergeng.o2o.enums;

/**
 * @author souper.geng
 * @date 2019/3/14
 */
public enum  ProductStateEnum {
    //错误类型
    ERROR(-1001, "内部系统错误"),
    //成功
    SUCCESS(1,"创建成功"),
    EMPTY(-1002,"添加数据少于1");

    private int state;
    private String stateInfo;

    public int getState() {
        return state;
    }

    public String getStateInfo() {
        return stateInfo;
    }

    ProductStateEnum(int state, String stateInfo) {
        this.state = state;
        this.stateInfo = stateInfo;
    }
}
