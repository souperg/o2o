package com.soupergeng.o2o.enums;

/**
 *
 * @author SouperGkz
 * @date 2018/11/13
 */
public enum ProductCategoryStateEnum {
    //错误类型
    ERROR(-1001, "内部系统错误"),
    //成功
    SUCCESS(1,"创建成功"),
    EMPTY_LIST(-1002,"添加数据少于1");
    /**
     * 状态码
     */
    private int state;
    /**
     * 状态信息
     */
    private String stateInfo;

    ProductCategoryStateEnum(int state, String stateInfo) {
        this.state = state;
        this.stateInfo = stateInfo;
    }

    public int getState() {
        return state;
    }

    public String getStateInfo() {
        return stateInfo;
    }

    public static ProductCategoryStateEnum stateOf(int index ){
        for (ProductCategoryStateEnum state:values()) {
            if (state.getState() == index){
                return state;
            }
        }
        return null;
    }
}
