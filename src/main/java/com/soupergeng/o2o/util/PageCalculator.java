package com.soupergeng.o2o.util;

/**
 *
 * @author souper.geng
 * @date 2018/11/23
 */
public class PageCalculator {
    /**
     * 计算数据库页码
     * @param pageIndex
     * @param pageSize
     * @return
     */
    public static int calculatorRowIndex(int pageIndex, int pageSize) {
        return (pageIndex>0) ? (pageIndex-1) * pageSize :0;
    }
}
