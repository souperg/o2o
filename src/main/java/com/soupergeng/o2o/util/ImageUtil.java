package com.soupergeng.o2o.util;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * Created by SouperGkz on 2018/11/12.
 */
public class ImageUtil {
    public static String basePath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
    private static Random random = new Random();

    public static void main(String[] args) throws Exception {
//        Thumbnails.of(new File(basePath+"/watermark.png")).size(20,20)
        Thumbnails.of(new File(basePath + "/image/xingkong.jpg")).size(320, 320)
                .watermark(Positions.BOTTOM_RIGHT, ImageIO.read(new File(basePath + "/watermark.png")), 0.25f)
                .outputQuality(1f)
//                .toFile(basePath+"/watermarknew.png");
                .toFile(basePath + "/image/xingkongnew.jpg");
//        .watermark(Positions.BOTTOM_RIGHT,ImageIO.read(new File(basePath+"/watermark.png")),0.25f)
    }

    public static String generateThumbnail(MultipartFile files, String targetAddr) {
        String realFileName = getRandomFileName();
        String extension = getFileExtension(files);
        makedirPath(targetAddr);
        String reallyPath = targetAddr + realFileName + extension;
        File dest = new File(PathUtil.getImgBasePath() + reallyPath);
        try {
            Thumbnails.of(files.getInputStream()).size(200, 200).watermark(Positions.BOTTOM_RIGHT, ImageIO.read(new File(basePath + "/watermark.png")), 0.25f)
                    .outputQuality(0.8f)
                    .toFile(dest);
        } catch (Exception e) {
            throw new RuntimeException("创建缩略图失败：" + e.toString());
        }
        return reallyPath;
    }

    public static String generateNormalImage(MultipartFile files, String targetAddr) {
        String realFileName = getRandomFileName();
        String extension = getFileExtension(files);
        makedirPath(targetAddr);
        String reallyPath = targetAddr + realFileName + extension;
        File dest = new File(PathUtil.getImgBasePath() + reallyPath);
        try {
            Thumbnails.of(files.getInputStream()).size(337, 640).watermark(Positions.BOTTOM_RIGHT, ImageIO.read(new File(basePath + "/watermark.png")), 0.25f)
                    .outputQuality(0.9f)
                    .toFile(dest);
        } catch (Exception e) {
            throw new RuntimeException("创建缩略图失败：" + e.toString());
        }
        return reallyPath;
    }

    /**
     * 创造目标路径所在的目录
     *
     * @param targetAddr
     */
    private static void makedirPath(String targetAddr) {
        String realFileParentPath = PathUtil.getImgBasePath() + targetAddr;
        File dirPath = new File(realFileParentPath);
        if (!dirPath.exists()) {
            dirPath.mkdirs();
        }
    }

    /**
     * 获取输入流的扩展名
     *
     * @param files
     * @return
     */
    private static String getFileExtension(MultipartFile files) {
        String origanal = files.getOriginalFilename();
        return origanal.substring(origanal.lastIndexOf("."));
    }

    /**
     * 生成随机文件名
     *
     * @return
     */
    private static String getRandomFileName() {
        //获取随机五位数
        int r = random.nextInt(89999) + 10000;
        String timeT = simpleDateFormat.format(new Date());
        return timeT + r;
    }

    /**
     * 如果是目录删除目录下所有文件
     * 如果是文件删除文件
     *
     * @param storePath
     */
    public static void deleteFileOrPath(String storePath) {
        File file = new File(PathUtil.getImgBasePath() + storePath);
        if (file.exists()) {
            if (file.isDirectory()) {
                File files[] = file.listFiles();
                for (File f : files) {
                    f.delete();
                }

            }


            file.delete();

        }

    }
}
