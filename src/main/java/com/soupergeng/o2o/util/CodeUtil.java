package com.soupergeng.o2o.util;

import javax.servlet.http.HttpServletRequest;

import static com.google.code.kaptcha.Constants.KAPTCHA_SESSION_KEY;

/**
 * 验证验证码
 * Created by souper.geng on 2018/11/19.
 */
public class CodeUtil {
    public static boolean checkVerifyCode(HttpServletRequest request){
        String code = (String) request.getSession().getAttribute(KAPTCHA_SESSION_KEY);
        String codeActual = HttpServletRequestUtil.getString(request,"verifyCodeActual");
        return code!=null && code.equals(codeActual);
    }
}
