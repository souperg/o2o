package com.soupergeng.o2o.util;


import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * 处理HttpServletRequest参数
 * Created by SouperGkz on 2018/11/14.
 */
public class HttpServletRequestUtil {
    public static int getInt(HttpServletRequest request, String key) {
        try {
            return Integer.decode(request.getParameter(key));
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static Long getLong(HttpServletRequest request, String key) {
        try {
            return Long.valueOf(request.getParameter(key));
        } catch (Exception e) {
            e.printStackTrace();
            return -1L;
        }
    }

    public static Double getDouble(HttpServletRequest request, String key) {
        try {
            return Double.valueOf(request.getParameter(key));
        } catch (Exception e) {
            e.printStackTrace();
            return -1d;
        }
    }

    public static Boolean getBoolean(HttpServletRequest request, String key) {
        try {
            return Boolean.valueOf(request.getParameter(key));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String getString(HttpServletRequest request, String key) {
        try{
            String result = request.getParameter(key);
            if (!StringUtils.isEmpty(result)) {
                result = result.trim();
            }
            if (StringUtils.isEmpty(result)) {
                result = null;
            }
            return result;
        } catch (Exception e){
            return null;
        }
    }
}
