package com.soupergeng.o2o.util;

/**
 * 地址工具类
 * Created by SouperGkz on 2018/11/13.
 */
public class PathUtil {
    public static final String separator = System.getProperty("file.separator");
    public static String  getImgBasePath() {
        String os = System.getProperty("os.name");
        String basePath = "";
        if (os.toLowerCase().startsWith("win")){
            basePath = "D:/app/image";
        } else {
            //mac 系统
            basePath = "/Users/souper.geng/myproject/o2o/image";
        }
        basePath = basePath.replace("/",separator);
        return basePath;
    }

    public static String getShopImagePath(long shopId){
        String imagePath = "/upload/item/shop/"+shopId+"/";
        imagePath.replace("/",separator);
        return imagePath;
    }
}
