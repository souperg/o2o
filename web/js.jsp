<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
</head>
<body>
<div>
    <h1>Native&JavaScript</h1>
</div>

<div>
    <button onclick="callShare()">share</button>
</div>

<script>
    var callShare = function() {
        <!--这里对iOS和Android进行一个判别-->
        var ua = navigator.userAgent.toLowerCase();
        if (/iphone|ipad|ipod/.test(ua)) {
            <!--如需传给oc参数，如数组，则写法为 "window.webkit.messageHandlers.share.postMessage([1, 'string'])"-->
            window.webkit.messageHandlers.share.postMessage(null)
        } else if (/android/.test(ua)) {
            <!--ps: "android"为安卓代码中必须与js保持一致的标识符，可自行定义-->
            window.jsCaller.hello("Hello World")
        }
    }
</script>
</body>
</html>