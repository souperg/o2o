$(function () {

    /**
     * 列表url
     * @type {string}
     */
    var getProductListUrl = '/o2o/product/getproductlistbyshop?pageIndex=1&pageSize=3';
    /**
     * 修改商品上下架状态url
     * @type {string}
     */
    var statusChangeUrl = '/o2o/product/modifyproduct';

    getProductList();


    function compactProduct(productId, enableStatus,  textOp) {
        var html = '<a href="#" class="edit" data-id="' + productId + '" data-status="' + enableStatus + '">编辑</a>' +
            '<a href="#" class="status" data-id="' + productId + '" data-status="' + enableStatus + '">' + textOp + '</a>' +
            '<a href="#" class="preview" data-id="' + productId + '" data-status="' + enableStatus + '">预览</a>';
        return html;
    }

    function handleShopList(productList) {
        var htmlItem = '<div class="row row-product">' +
            '<div class="col-33 shop-name">{0}</div>' +
            '<div class="col-20">{1}</div>' +
            '<div class="col-40">{2}</div>' +
            '</div>';
        var html = '';
        productList.map(function (item, index) {
            var textOp = "下架";
            var tempStatus = 1;
            if (item.enableStatus == 0) {
                textOp = "上架";
            } else {
                tempStatus = 0;
            }
            html += htmlItem.format(item.productName, item.priority, compactProduct(item.productId, item.enableStatus, textOp));
        });
        $('.product-wrap').html(html);
    }

    function getProductList() {
        $.getJSON(getProductListUrl, function (data) {
            if (data.success) {
                handleShopList(data.productList);
            } else {
                $.toast(data.errMsg);
            }
        });
    }

    function changeItemStatus(id, status) {
        var product = {};
        product.productId = id;
        product.enableStatus = status;
        $.confirm('确定要修改吗?', function () {
            //上下架相关操作
            $.ajax({
                url: statusChangeUrl,
                type: 'POST',
                data: {
                    productStr: JSON.stringify(product),
                    statusChange: true
                },
                dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        $.toast('操作成功!');
                        getProductList();
                    } else {
                        $.toast('操作失败!');
                    }
                }
            });
        })
    }

    //绑定a标签点击事件
    $('.product-wrap').on('click', 'a', function (e) {

        if (($(e.currentTarget)).hasClass('edit')) {
            //跳转编辑页面,并带有productId参数
            window.location.href = '/o2o/product/productmanage?productId=' + e.currentTarget.dataset.id;
        }
        if(($(e.currentTarget)).hasClass('status')){
            changeItemStatus(e.currentTarget.dataset.id, e.currentTarget.dataset.status);
        }
        if(($(e.currentTarget)).hasClass('preview')){
            //TODO 前端预览待开发
            window.location.href = '/o2o/frontend/productdetail?productId=' + e.currentTarget.dataset.id;
        }
    });
});