$(
    /**
     *
     */
    function () {
        var productId = getQueryString("productId");
        var isEdit = !!productId;
        var categoryUrl = '/o2o/productcategory/getproductcategorylist';
        //修改商品信息
        var postProductUrl = '/o2o/product/modifyproduct';
        //获取商品信息
        var getProductInfoUrl = '/o2o/product/getproductbyid?productId=' + productId;

        if (isEdit) {
            //编辑页面
            getProductInfo(productId);

        } else {
            //获取商品类别
            $('.title').html("商品添加");
            getProductCategory();
            postProductUrl = '/o2o/product/addproduct';
        }

        /**
         * 获取商品信息,赋值给表单
         */
        function getProductInfo(id) {
            $.getJSON(getProductInfoUrl, function (data) {
                if (data.success) {
                    var product = data.product;
                    $('#product-name').val(product.productName);
                    $('#priority').val(product.priority);
                    $('#product-desc').val(product.productDesc);
                    $('#normal-price').val(product.normalPrice);
                    $('#promotion-price').val(product.promotionPrice);
                    //获取原本商品类别以及该店铺所有的商品类别列表
                    var productCategory = '<option data-id = "' + product.productCategory.productCategoryId + '">'
                        + product.productCategory.productCategoryName + '</option>';
                    var tempCategoryHtml = '';
                    var categoryArr = data.productCategoryList;
                    var categorySelectd = product.productCategory.productCategoryId;
                    //填充数据
                    categoryArr.map(function (item, index) {
                        var isSelected = categorySelectd === item.productCategoryId ? 'selected' : '';
                        tempCategoryHtml += '<option data-value="' +
                            item.productCategoryId +
                            '"' +
                            isSelected +
                            '>' +
                            item.productCategoryName +
                            '</option>';
                    });
                    $('#category').html(tempCategoryHtml);
                }
            });
        }

        /**
         * 为商品添加操作提供该店铺下的所有商品类别列表
         */
        function getProductCategory() {
            $.getJSON(categoryUrl, function (data) {
                if (data.success) {
                    var productCategoryList = data.data;
                    var tempCategoryHtml;
                    productCategoryList.map(function (item, index) {
                        tempCategoryHtml += '<option data-value="' +
                            item.productCategoryId +
                            '">' + item.productCategoryName + '</option>';
                    });
                    $('#category').html(tempCategoryHtml);
                }
            });
        }

        /**
         * 针对商品详情图控件组,若组的最后一个元素发生变化(即上传了图片)
         * 且控件数未达到6个,则生成一个新的上传控件
         */
        $('.detail-img-div').on('change', '.detail-img:last-child', function () {
            if ($('.detail-img').length < 6) {
                $('#detail-img').append('<input type="file" class="detail-img">');
            }
        });


        /**
         * 提交表单
         */
        $('#submit').click(function () {
            var product = {};
            if (isEdit) {
                product.productId = productId;
            }
            product.productName = $('#product-name').val();
            product.productDesc = $('#product-desc').val();
            product.priority = $('#priority').val();
            product.normalPrice = $('#normal-price').val();
            product.promotionPrice = $('#promotion-price').val();
            //商品类别
            product.productCategory = {
                productCategoryId: $('#category').find('option').not(function () {
                    return !this.selected;
                }).data('value')
            };
            product.productId = productId;

            //缩略图
            var thumbnail = $('#small-img')[0].files[0];
            //生成表单数据
            var formData = new FormData();
            formData.append("thumbnail", thumbnail);
            // 遍历商品详情图片控件,获取里边的文件流
            $('.detail-img').map(function (index, item) {
                if ($('.detail-img')[index].files.length > 0) {
                    formData.append('productImg'+index,$('.detail-img')[index].files[0]);
                }
            });
            var verifyCodeActual = $('#j-captche').val();
            if (!verifyCodeActual) {
                $.toast('请输入验证码!');
                return;
            }
            formData.append("verifyCodeActual", verifyCodeActual);
            //将product json对象转成字符流保存至保单对象key为productStr的键值对中
            formData.append('productStr',JSON.stringify(product));
            $.ajax({
                url: postProductUrl,
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                cache: false,
                success: function (data) {
                    if (data.success) {
                        $.toast('提交成功!');
                    } else {
                        $.toast('提交失败'+data.errMsg);
                    }
                    $('#captcha-img').click();
                }

            })
        });


    });