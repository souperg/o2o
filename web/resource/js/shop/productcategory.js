/**
 * Created by souper.geng on 2018/12/5.
 */
$(function () {
    var listUrl = '/o2o/productcategory/getproductcategorylist';
    var addUrl = '/o2o/productcategory/addproductcategorys';
    var deleteUrl = '/o2o/productcategory/removeproductcategory';
    getProductioncategoryList();
    /**
     * 填充数据
     * @param dataList
     */
    function contentCategory(dataList) {
        var htmlItem = '<div class="row row-product-category now">' +
            '<div class="col-33 product-category-name">{0}</div>' +
            '<div class="col-33">{1}</div>' +
            '<div class="col-33" ><a href="#" class="button delete" data-id="{2}">删除</a></div>' +
            '</div>';
        var html = '';
        dataList.map(function (item, index) {
            html += htmlItem.format(item.productCategoryName, item.priority, item.productCategoryId);
        });
        $('.productcategory-wrap').html(html);

    }

    /**
     * 获取数据
     */
    function getProductioncategoryList() {
        $.getJSON(listUrl, function (data) {
            if (data.success) {
                contentCategory(data.data);
            } else {

            }

        })
    }

    $('#new').click(function () {
        var htmlItem = '<div class="row row-product-category temp">' +
            '<div class="col-33"><input class="category-input category" type="text" placeholder="店铺类别"></div>' +
            '<div class="col-33"><input class="category-input priority" type="number" placeholder="优先级"></div>' +
            '<div class="col-33" ><a href="#" class="button delete">删除</a></div>' +
            '</div>';
        $('.productcategory-wrap').append(htmlItem);
    });
    $('#submit').click(function () {
        var tempArr = $(".temp");
        var productCategoryList = [];
        tempArr.map(function (index, item) {
            var temoObj = {};
            temoObj.productCategoryName = $(item).find('.category').val();
            temoObj.priority = $(item).find('.priority').val();
            if (temoObj.productCategoryName && temoObj.priority) {
                productCategoryList.push(temoObj);
            }
        });

        $.ajax({
            url: addUrl,
            type: 'POST',
            data: JSON.stringify(productCategoryList),
            contentType : 'application/json',
            success : function (data) {
                if (data.success){
                    $.toast("提交成功!");
                    getProductioncategoryList();
                } else{
                    $.toast(data.errMsg);
                }
            }
        });

    });

    $('.productcategory-wrap').on('click','.row-product-category.temp .delete',function (e) {
        console.log($(this).parent().parent());
        $(this).parent().parent().remove();
    });
    $('.productcategory-wrap').on('click','.row-product-category.now .delete',function (e) {
        var target = e.currentTarget;
        $.confirm('确定删除吗?',function () {
            $.ajax({
                url : deleteUrl,
                type : 'POST',
                data : {productCategoryId : target.dataset.id},
                dataType : 'json',
                success : function (data) {
                    if (data.success){
                        $.toast("删除成功!");
                        getProductioncategoryList();
                    } else {
                        $.toast(data.errMsg);
                    }
                }
            });
        });
    });


});