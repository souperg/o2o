$(function () {

    var getShopListUrl = '/o2o/shopadmin/getshoplist';

    getShopList();

    var shopStatus = function (enableStatus) {
        if (0 == enableStatus) {
            return '审核中';
        } else if (-1 == enableStatus) {
            return '店铺非法';
        } else {
            return '审核通过';
        }
    };

    function goShop(enableStatus, shopId) {
        if (enableStatus == 1) {
            var html = '<a href="/o2o/shopadmin/shopmanagement?shopId=' + shopId + '">进入</a>';
            return html;
        } else {
            return '';
        }

    }

    function handleShopList(shopList) {
        var htmlItem = '<div class="row row-shop">' +
            '<div class="col-40 shop-name">{0}</div>' +
            '<div class="col-40">{1}</div>' +
            '<div class="col-20">{2}</div>' +
            '</div>';
        var html='';
        shopList.map(function (item, index) {
            html += htmlItem.format(item.shopName, shopStatus(item.enableStatus), goShop(item.enableStatus, item.shopId));
        });
        $('#shop-wrap').html(html);
    }

    function handleUser(user) {
        $('#user-name').text(user.name);

    }

    function getShopList() {
        // $.ajax({
        //     url: getShopListUrl,
        //     type: 'GET',
        //     data: 'json',
        //     success: function (data) {
        //         if (data.success) {
        //              handleShopList(data.shopList);
        //              handleUser(data.user);
        //         } else {
        //             $.toast('获取失败' + data.msg);
        //         }
        //     }
        //
        // })
        $.getJSON(getShopListUrl, function (data) {
            if (data.success) {
                handleShopList(data.shopList);
                handleUser(data.user);
            } else {
                alert(data.errMsg);
            }
        });
    }

    // $('#submit').click(function () {
    //     var shop = {};
    //     if (isEdit){
    //         shop.shopId = shopId;
    //     }
    //     shop.shopName = $('#shop-name').val();
    //     shop.shopAddr = $('#shop-addr').val();
    //     shop.phone = $('#shop-phone').val();
    //     shop.shopDesc = $('#shop-desc').val();
    //     shop.shopCategory = {
    //         shopCategoryId: $('#shop-category').find('option').not(function () {
    //             return !this.selected;
    //         }).data('id')
    //     };
    //     shop.area = {
    //         areaId: $('#area').find('option').not(function () {
    //             return !this.selected;
    //         }).data('id')
    //     };
    //     var shopImg = $('#shop-img')[0].files[0];
    //     var formData = new FormData();
    //     formData.append("shopImg", shopImg);
    //     formData.append("shopStr", JSON.stringify(shop));
    //     var verifyCodeActual = $('#j-captche').val();
    //     if (!verifyCodeActual) {
    //         $.toast('请输入验证码!');
    //         return;
    //     }
    //     formData.append("verifyCodeActual", verifyCodeActual);
    //     $.ajax({
    //         url: (!isEdit ? registerShopUrl : updateShopUrl),
    //         type: 'POST',
    //         data: formData,
    //         contentType: false,
    //         processData: false,
    //         cache: false,
    //         success: function (data) {
    //             if (data.success) {
    //                 $.toast('提交成功!');
    //             } else {
    //                 $.toast('提交失败' + data.msg);
    //             }
    //             $('#captcha-img').click();
    //         }
    //
    //     })
    // });


});