/**
 * 商铺管理
 * Created by souper.geng on 2018/12/3.
 */

$(function () {
    var shopId = getQueryString("shopId");
    var shopInfoUrl = '/o2o/shopadmin/getshopmanagermentinfo?shopId='+shopId;
    
    $.getJSON(shopInfoUrl,function (data) {
        if (data.redirect){
            window.location.href = data.url;

        } else {
            $("#shopInfo").attr("href","/o2o/shopadmin/shopoperation?shopId="+shopId);
        }
    })

});