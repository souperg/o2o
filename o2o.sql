/*
 Navicat Premium Data Transfer

 Source Server         : gkz
 Source Server Type    : MySQL
 Source Server Version : 80011
 Source Host           : localhost:3306
 Source Schema         : o2o

 Target Server Type    : MySQL
 Target Server Version : 80011
 File Encoding         : 65001

 Date: 14/03/2019 17:23:23
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_area
-- ----------------------------
DROP TABLE IF EXISTS `tb_area`;
CREATE TABLE `tb_area` (
  `area_id` int(11) NOT NULL AUTO_INCREMENT,
  `area_name` varchar(200) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  `last_edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`area_id`),
  UNIQUE KEY `UK_AREA` (`area_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_area
-- ----------------------------
BEGIN;
INSERT INTO `tb_area` VALUES (1, '西苑', 2, NULL, NULL);
INSERT INTO `tb_area` VALUES (2, '东苑', 1, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for tb_head_line
-- ----------------------------
DROP TABLE IF EXISTS `tb_head_line`;
CREATE TABLE `tb_head_line` (
  `line_id` int(100) NOT NULL AUTO_INCREMENT,
  `line_name` varchar(1000) DEFAULT NULL,
  `line_link` varchar(2000) NOT NULL,
  `line_img` varchar(2000) NOT NULL,
  `priority` int(2) DEFAULT NULL,
  `enable_status` int(2) NOT NULL DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  `last_edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`line_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tb_interface_log
-- ----------------------------
DROP TABLE IF EXISTS `tb_interface_log`;
CREATE TABLE `tb_interface_log` (
  `log_id` int(20) NOT NULL AUTO_INCREMENT,
  `interface_name` varchar(1000) NOT NULL,
  `used_time` varchar(1000) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `note1` varchar(1000) DEFAULT NULL,
  `note2` varchar(1000) DEFAULT NULL,
  `note3` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_interface_log
-- ----------------------------
BEGIN;
INSERT INTO `tb_interface_log` VALUES (1, '/shopadmin/shopoperation', '1000', '2018-11-23 07:10:02', NULL, NULL, NULL);
INSERT INTO `tb_interface_log` VALUES (2, '/shopadmin/shopoperation', '1000', '2019-03-04 08:29:26', NULL, NULL, NULL);
INSERT INTO `tb_interface_log` VALUES (3, '/shopadmin/shopoperation', '1000', '2019-03-05 05:58:17', NULL, NULL, NULL);
INSERT INTO `tb_interface_log` VALUES (4, '/shopadmin/shopoperation', '1000', '2019-03-05 05:58:54', NULL, NULL, NULL);
INSERT INTO `tb_interface_log` VALUES (5, '/shopadmin/shopoperation', '1000', '2019-03-05 05:59:54', NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for tb_local_auth
-- ----------------------------
DROP TABLE IF EXISTS `tb_local_auth`;
CREATE TABLE `tb_local_auth` (
  `local_auth_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `user_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `last_edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`local_auth_id`),
  UNIQUE KEY `uk_local_profile` (`user_name`),
  KEY `fk_local_profile` (`user_id`),
  CONSTRAINT `fk_local_profile` FOREIGN KEY (`user_id`) REFERENCES `tb_person_info` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for tb_person_info
-- ----------------------------
DROP TABLE IF EXISTS `tb_person_info`;
CREATE TABLE `tb_person_info` (
  `user_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `gender` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `profile_img` varchar(1024) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_flag` int(2) NOT NULL DEFAULT '0',
  `shop_owner_flag` int(2) NOT NULL DEFAULT '0',
  `admin_flag` int(2) NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `last_edit_time` datetime DEFAULT NULL,
  `enable_status` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tb_person_info
-- ----------------------------
BEGIN;
INSERT INTO `tb_person_info` VALUES (1, '郝蝶', NULL, '1', '17600220515', 'test', NULL, 0, 0, 0, '2018-11-19 21:40:48', NULL, 0);
COMMIT;

-- ----------------------------
-- Table structure for tb_product
-- ----------------------------
DROP TABLE IF EXISTS `tb_product`;
CREATE TABLE `tb_product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(1000) NOT NULL,
  `product_desc` varchar(2000) DEFAULT NULL,
  `img_addr` varchar(2000) DEFAULT '',
  `normal_price` varchar(128) DEFAULT NULL,
  `promotion_price` varchar(128) DEFAULT NULL,
  `priority` int(2) NOT NULL DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  `last_edit_time` datetime DEFAULT NULL,
  `enable_status` int(2) NOT NULL DEFAULT '0',
  `product_category_id` int(11) DEFAULT NULL,
  `shop_id` int(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`),
  KEY `fk_product_procate` (`product_category_id`),
  KEY `fk_product_shop` (`shop_id`),
  CONSTRAINT `fk_product_procate` FOREIGN KEY (`product_category_id`) REFERENCES `tb_product_category` (`product_category_id`),
  CONSTRAINT `fk_product_shop` FOREIGN KEY (`shop_id`) REFERENCES `tb_shop` (`shop_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_product
-- ----------------------------
BEGIN;
INSERT INTO `tb_product` VALUES (2, '外卖', '外卖', '', NULL, NULL, 0, NULL, NULL, 0, 5, 8);
INSERT INTO `tb_product` VALUES (3, '奶茶', '奶茶妹妹的小茶店', '图片', '5.0', '6.0', 1, '2019-03-13 10:05:44', NULL, 1, NULL, 8);
INSERT INTO `tb_product` VALUES (6, '测试商品', '测试商品111z', '/upload/item/shop/8/2019031416263881241.jpg', NULL, NULL, 10, '2019-03-14 08:26:38', '2019-03-14 08:26:38', 1, 5, 8);
COMMIT;

-- ----------------------------
-- Table structure for tb_product_category
-- ----------------------------
DROP TABLE IF EXISTS `tb_product_category`;
CREATE TABLE `tb_product_category` (
  `product_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_category_name` varchar(1000) NOT NULL DEFAULT '',
  `priority` int(2) NOT NULL DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  `last_edit_time` datetime DEFAULT NULL,
  `shop_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_category_id`),
  KEY `fk_procate_shop` (`shop_id`),
  CONSTRAINT `fk_procate_shop` FOREIGN KEY (`shop_id`) REFERENCES `tb_shop` (`shop_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_product_category
-- ----------------------------
BEGIN;
INSERT INTO `tb_product_category` VALUES (5, '店铺类别1', 1, NULL, NULL, 8);
INSERT INTO `tb_product_category` VALUES (7, '店铺类别2', 2, NULL, NULL, 8);
INSERT INTO `tb_product_category` VALUES (9, '店铺类别3', 3, NULL, NULL, 8);
INSERT INTO `tb_product_category` VALUES (10, '店铺类别4', 10, NULL, NULL, 8);
INSERT INTO `tb_product_category` VALUES (11, '店铺类别5', 0, NULL, NULL, 9);
COMMIT;

-- ----------------------------
-- Table structure for tb_product_img
-- ----------------------------
DROP TABLE IF EXISTS `tb_product_img`;
CREATE TABLE `tb_product_img` (
  `product_img_id` int(20) NOT NULL AUTO_INCREMENT,
  `img_addr` varchar(1000) NOT NULL,
  `img_desc` varchar(1000) DEFAULT NULL,
  `priority` int(2) NOT NULL DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  `product_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_img_id`),
  KEY `fk_proimg_product` (`product_id`),
  CONSTRAINT `fk_proimg_product` FOREIGN KEY (`product_id`) REFERENCES `tb_product` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_product_img
-- ----------------------------
BEGIN;
INSERT INTO `tb_product_img` VALUES (3, '图片2', '图片2', 2, '2019-03-13 09:57:00', 2);
INSERT INTO `tb_product_img` VALUES (4, '图片2', '图片2', 2, '2019-03-13 09:57:00', 2);
INSERT INTO `tb_product_img` VALUES (5, '/upload/item/shop/8/2019031416263891815.jpg', NULL, 6, '2019-03-14 08:26:39', 6);
INSERT INTO `tb_product_img` VALUES (6, '/upload/item/shop/8/2019031416263866050.jpg', NULL, 6, '2019-03-14 08:26:39', 6);
COMMIT;

-- ----------------------------
-- Table structure for tb_shop
-- ----------------------------
DROP TABLE IF EXISTS `tb_shop`;
CREATE TABLE `tb_shop` (
  `shop_id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL COMMENT '店铺创始人',
  `area_id` int(11) DEFAULT NULL,
  `shop_category_id` int(11) DEFAULT NULL,
  `shop_name` varchar(1000) NOT NULL,
  `shop_desc` varchar(2000) DEFAULT NULL,
  `shop_addr` varchar(2000) DEFAULT NULL,
  `phone` varchar(128) DEFAULT NULL,
  `shop_img` varchar(2000) DEFAULT NULL,
  `priority` int(3) DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  `last_edit_time` datetime DEFAULT NULL,
  `enable_status` int(2) NOT NULL DEFAULT '0',
  `advice` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`shop_id`),
  KEY `fk_shop_area` (`area_id`),
  KEY `fk_shop_profile` (`owner_id`),
  KEY `fk_shop_category` (`shop_category_id`),
  CONSTRAINT `fk_shop_area` FOREIGN KEY (`area_id`) REFERENCES `tb_area` (`area_id`),
  CONSTRAINT `fk_shop_category` FOREIGN KEY (`shop_category_id`) REFERENCES `tb_shop_category` (`shop_category_id`),
  CONSTRAINT `fk_shop_profile` FOREIGN KEY (`owner_id`) REFERENCES `tb_person_info` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_shop
-- ----------------------------
BEGIN;
INSERT INTO `tb_shop` VALUES (8, 1, 1, 2, '正式咖啡馆', '哈哈哈哈哈', '北京', '17600220515', '/upload/item/shop/8/2018112214123760791.jpg', NULL, '2018-11-19 14:04:17', '2018-11-22 06:12:37', 0, NULL);
INSERT INTO `tb_shop` VALUES (9, 1, 1, 2, 'hhhh', 'hehehhehehehe', 'hhhhh', '7777777', '/upload/item/shop/9/2018111922055726370.jpg', NULL, '2018-11-19 14:05:57', '2018-11-19 14:05:57', 0, NULL);
INSERT INTO `tb_shop` VALUES (10, 1, 1, 2, 'lllll', 'qwrqwerqw', 'llllll', '1234213412', '/upload/item/shop/10/2018111922164068554.jpg', NULL, '2018-11-19 14:16:40', '2018-11-19 14:16:40', 0, NULL);
INSERT INTO `tb_shop` VALUES (11, 1, 1, 2, 'lllll', 'qwrqwerqw', 'llllll', '1234213412', '/upload/item/shop/11/2018111922201833183.jpg', NULL, '2018-11-19 14:20:18', '2018-11-19 14:20:18', 0, NULL);
INSERT INTO `tb_shop` VALUES (12, 1, 1, 2, 'hd', 'hahhahahahahah', 'hdhdhd', '16527284', '/upload/item/shop/12/2018111922214384375.gif', NULL, '2018-11-19 14:21:44', '2018-11-19 14:21:44', 0, NULL);
INSERT INTO `tb_shop` VALUES (13, 1, 2, 1, 'test的店铺1', 'test', 'test', '17600220510', 'test', 1, '2018-11-23 08:19:10', NULL, 1, '审核中');
INSERT INTO `tb_shop` VALUES (14, 1, 2, 1, 'test的店铺2', 'test', 'test', '17600220510', 'test', 1, '2018-11-23 08:19:22', NULL, 1, '审核中');
INSERT INTO `tb_shop` VALUES (15, 1, 2, 1, 'test的店铺3', 'test', 'test', '17600220510', 'test', 1, '2018-11-23 08:19:33', NULL, 1, '审核中');
INSERT INTO `tb_shop` VALUES (16, 1, 2, 1, 'test的店铺4', 'test', 'test', '17600220510', 'test', 1, '2018-11-23 08:19:44', NULL, 1, '审核中');
INSERT INTO `tb_shop` VALUES (17, 1, 2, 1, 'test的店铺4', 'test', 'test', '17600220510', 'test', 1, '2019-03-04 08:29:26', NULL, 1, '审核中');
INSERT INTO `tb_shop` VALUES (18, 1, 2, 1, 'test的店铺4', 'test', 'test', '17600220510', 'test', 1, '2019-03-05 05:58:17', NULL, 1, '审核中');
INSERT INTO `tb_shop` VALUES (19, 1, 2, 1, 'test的店铺4', 'test', 'test', '17600220510', 'test', 1, '2019-03-05 05:58:54', NULL, 1, '审核中');
INSERT INTO `tb_shop` VALUES (20, 1, 2, 1, 'test的店铺4', 'test', 'test', '17600220510', 'test', 1, '2019-03-05 05:59:54', NULL, 1, '审核中');
COMMIT;

-- ----------------------------
-- Table structure for tb_shop_category
-- ----------------------------
DROP TABLE IF EXISTS `tb_shop_category`;
CREATE TABLE `tb_shop_category` (
  `shop_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_category_name` varchar(1000) NOT NULL DEFAULT '',
  `shop_category_desc` varchar(2000) DEFAULT '',
  `shop_category_img` varchar(2000) DEFAULT NULL,
  `priority` int(2) NOT NULL DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  `last_edit_time` datetime DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`shop_category_id`),
  KEY `fk_shop_category_self` (`parent_id`),
  CONSTRAINT `fk_shop_category_self` FOREIGN KEY (`parent_id`) REFERENCES `tb_shop_category` (`shop_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_shop_category
-- ----------------------------
BEGIN;
INSERT INTO `tb_shop_category` VALUES (1, '咖啡奶茶', '真好喝', 'test', 1, '2018-11-16 18:58:38', NULL, NULL);
INSERT INTO `tb_shop_category` VALUES (2, '咖啡', '测试类别', 'test', 0, NULL, NULL, 1);
COMMIT;

-- ----------------------------
-- Table structure for tb_wechat_auth
-- ----------------------------
DROP TABLE IF EXISTS `tb_wechat_auth`;
CREATE TABLE `tb_wechat_auth` (
  `wechat_auth_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `open_id` varchar(512) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`wechat_auth_id`),
  KEY `fk_oauth_profile` (`user_id`),
  KEY `uk_oauth` (`open_id`(255)),
  CONSTRAINT `fk_oauth_profile` FOREIGN KEY (`user_id`) REFERENCES `tb_person_info` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

SET FOREIGN_KEY_CHECKS = 1;
